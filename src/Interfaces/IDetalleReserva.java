
package Interfaces;

import Datos.DetalleReserva;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

public interface IDetalleReserva {
    
    boolean insertar(DetalleReserva detRes);
    boolean editar(DetalleReserva detRes);
    boolean eliminar(DetalleReserva detRes);
    
    boolean verificar_disponibilidad(String fecha_ingreso,String fecha_salida,String numHab);
    int hallarItem(String id_reserva);
    DefaultComboBoxModel mostrarEstadoInicio();
    DefaultTableModel mostrar(String id_reserva,int item);
}
