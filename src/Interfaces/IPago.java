
package Interfaces;

import Datos.Pago;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

public interface IPago {
    
    boolean insertar(Pago pago);
    boolean eliminar(Pago pago);
    boolean consultar(String idPago);
    
    DefaultTableModel mostrarPagado(String idReserva);
    DefaultTableModel mostrarPendiente(String idReserva);
    
    DefaultComboBoxModel mostrarEstadoInicio();
    DefaultComboBoxModel mostrarTipoComprobante();
    DefaultComboBoxModel mostrarTipoPago();
    
}
