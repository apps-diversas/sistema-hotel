
package Interfaces;

import Datos.Empleado;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;


public interface IEmpleado {
    
    DefaultTableModel mostrar(String buscar);
    DefaultComboBoxModel mostrarPais();
    DefaultComboBoxModel mostrarEstadoEmpleado();
    
    boolean insertar (Empleado emp);
    boolean editar (Empleado emp);
    boolean eliminar (Empleado emp);
    Empleado login(String usuario,String password);
}
