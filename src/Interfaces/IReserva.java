package Interfaces;

import Datos.Reserva;
import javax.swing.DefaultComboBoxModel;

public interface IReserva {

    boolean insertar(Reserva res);
    boolean editar(Reserva res);
    boolean eliminar(Reserva res);
    boolean eliminarDetalle(Reserva res);
    boolean finalizarReserva(String idReserva);
    int PagosPendientes(String idReserva);
    
    boolean consultar(String idReserva);
    String capturaCodigoCliente(String idReserva);
    
    DefaultComboBoxModel mostrarEstadoInicio();
    double totalPagarDetalle(String idReserva);
    
}
