
package Interfaces;

import Datos.Cliente;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

public interface ICliente {
    
    DefaultTableModel mostrar(String buscar);
    DefaultTableModel mostrarVista(String buscar);
    DefaultComboBoxModel mostrarPaisCliente();
    DefaultComboBoxModel mostrarEstadoCliente();
    
    boolean insertar (Cliente cli);
    boolean editar (Cliente cli);
    boolean eliminar (Cliente cli);
    
}
