
package Interfaces;

import Datos.Habitacion;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

public interface IHabitacion {
    
    DefaultTableModel mostrar(String buscar);
    DefaultTableModel mostrarVista(String numero);
    
    DefaultComboBoxModel mostrarTipoHabitacion();
    String capturarIdTipoHabitacion(String descripcion);
    String capturarIdHabitacion(String numero);
    
    DefaultComboBoxModel mostrarHabitacionxTipo(String idTipoHab);
    double mostrarPrecioxTipoHab(String idTipoHab);
    
    DefaultComboBoxModel mostrarEstadoHabitacion();
    
    boolean insertar (Habitacion hab);
    boolean editar (Habitacion hab);
    boolean eliminar (Habitacion hab);
    
    boolean existeHabitacion(String idHab, int numero);
    boolean desocupar (Habitacion hab);
    boolean ocupar (Habitacion hab);
    
}
