
package Datos;

public class Habitacion {

    private String idHabitacion;
    private String tipo_habitacion;
    private int numero;
    private int piso;
    private String descripcion;
    private String estado;

    public Habitacion() {
    }

    public Habitacion(String idHabitacion, String tipo_habitacion, int numero, int piso, String descripcion, String estado) {
        this.idHabitacion = idHabitacion;
        this.tipo_habitacion = tipo_habitacion;
        this.numero = numero;
        this.piso = piso;
        this.descripcion = descripcion;
        this.estado = estado;
    }

    public String getIdHabitacion() {
        return idHabitacion;
    }

    public void setIdHabitacion(String idHabitacion) {
        this.idHabitacion = idHabitacion;
    }

    public String getTipo_habitacion() {
        return tipo_habitacion;
    }

    public void setTipo_habitacion(String tipo_habitacion) {
        this.tipo_habitacion = tipo_habitacion;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getPiso() {
        return piso;
    }

    public void setPiso(int piso) {
        this.piso = piso;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
