
package Datos;

public class Pago {

    private String idPago;
    
    private String idTipoPago;
    private int nroPago;
    private String idTipoComp;
    private int nroComprobante;
    
    private String idReserva;
    private String flag_confirma;
    
    private String fechaEmision;
    private double subTotal;
    private double igv;
    private double total;
    private String idEstado;

    public Pago() {
    }

    public Pago(String idPago, String idTipoPago, int nroPago, String idTipoComp, int nroComprobante, String idReserva, String flag_confirma, String fechaEmision, double subTotal, double igv, double total, String idEstado) {
        this.idPago = idPago;
        this.idTipoPago = idTipoPago;
        this.nroPago = nroPago;
        this.idTipoComp = idTipoComp;
        this.nroComprobante = nroComprobante;
        this.idReserva = idReserva;
        this.flag_confirma = flag_confirma;
        this.fechaEmision = fechaEmision;
        this.subTotal = subTotal;
        this.igv = igv;
        this.total = total;
        this.idEstado = idEstado;
    }

    public String getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(String idEstado) {
        this.idEstado = idEstado;
    }

    public String getIdPago() {
        return idPago;
    }

    public void setIdPago(String idPago) {
        this.idPago = idPago;
    }

    public String getIdTipoPago() {
        return idTipoPago;
    }

    public void setIdTipoPago(String idTipoPago) {
        this.idTipoPago = idTipoPago;
    }

    public int getNroPago() {
        return nroPago;
    }

    public void setNroPago(int nroPago) {
        this.nroPago = nroPago;
    }

    public String getIdTipoComp() {
        return idTipoComp;
    }

    public void setIdTipoComp(String idTipoComp) {
        this.idTipoComp = idTipoComp;
    }

    public int getNroComprobante() {
        return nroComprobante;
    }

    public void setNroComprobante(int nroComprobante) {
        this.nroComprobante = nroComprobante;
    }

    public String getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(String idReserva) {
        this.idReserva = idReserva;
    }

    public String getFlag_confirma() {
        return flag_confirma;
    }

    public void setFlag_confirma(String flag_confirma) {
        this.flag_confirma = flag_confirma;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getIgv() {
        return igv;
    }

    public void setIgv(double igv) {
        this.igv = igv;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
}
