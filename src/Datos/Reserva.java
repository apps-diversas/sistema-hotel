
package Datos;

import java.sql.Date;

public class Reserva {

    private String idReserva;
    private String idCliente;
    private String fechaRegistro;
    private double total;
    private String idEstado;
    private String idEmpleado;
    private String flagConfirma;
    private double totalConfirma;

    public Reserva() {
    }

    public Reserva(String idReserva, String idCliente, String fechaRegistro, double total, String idEstado, String idEmpleado, String flagConfirma, double totalConfirma) {
        this.idReserva = idReserva;
        this.idCliente = idCliente;
        this.fechaRegistro = fechaRegistro;
        this.total = total;
        this.idEstado = idEstado;
        this.idEmpleado = idEmpleado;
        this.flagConfirma = flagConfirma;
        this.totalConfirma = totalConfirma;
    }

    public double getTotalConfirma() {
        return totalConfirma;
    }

    public void setTotalConfirma(double totalConfirma) {
        this.totalConfirma = totalConfirma;
    }

    public String getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(String idReserva) {
        this.idReserva = idReserva;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(String idEstado) {
        this.idEstado = idEstado;
    }

    public String getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(String idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getFlagConfirma() {
        return flagConfirma;
    }

    public void setFlagConfirma(String flagConfirma) {
        this.flagConfirma = flagConfirma;
    }
    
}
