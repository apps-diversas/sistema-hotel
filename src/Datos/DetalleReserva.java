
package Datos;

public class DetalleReserva {

    private String id_reserva;
    private int item;
    private String id_habitacion;
    private String fecha_ingreso;
    private String fecha_salida;
    private double importe;
    private double descuento;
    private String id_estado;

    public DetalleReserva() {
    }

    public DetalleReserva(String id_reserva, int item, String id_habitacion, String fecha_ingreso, String fecha_salida, double importe, double descuento, String id_estado) {
        this.id_reserva = id_reserva;
        this.item = item;
        this.id_habitacion = id_habitacion;
        this.fecha_ingreso = fecha_ingreso;
        this.fecha_salida = fecha_salida;
        this.importe = importe;
        this.descuento = descuento;
        this.id_estado = id_estado;
    }

    public String getId_estado() {
        return id_estado;
    }

    public void setId_estado(String id_estado) {
        this.id_estado = id_estado;
    }

    public String getId_reserva() {
        return id_reserva;
    }

    public void setId_reserva(String id_reserva) {
        this.id_reserva = id_reserva;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public String getId_habitacion() {
        return id_habitacion;
    }

    public void setId_habitacion(String id_habitacion) {
        this.id_habitacion = id_habitacion;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getFecha_salida() {
        return fecha_salida;
    }

    public void setFecha_salida(String fecha_salida) {
        this.fecha_salida = fecha_salida;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }
    
    
    
}
