package Presentacion;

import Dao.DetalleReservaDao;
import Dao.EstadoDao;
import Dao.HabitacionDao;
import Dao.ReservaDao;
import Datos.DetalleReserva;
//import Datos.Habitacion;
import static Presentacion.FrmInicio.escritorio;
import static Presentacion.IFrmReserva.txt_idReserva;
import Utilidades.Conexion;
//import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import java.sql.Connection;
//import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.soap.Detail;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

public class IFrmDetalleReserva extends javax.swing.JInternalFrame {

    public IFrmDetalleReserva() {
        initComponents();
        inhabilitar();
        txt_idreserva.setText(IFrmReserva.idReserva);
        mostrarDetalles(txt_idreserva.getText(), 0);
        cargarEstado();
        
    }

    String accion = "guardar";
    Connection cn = Conexion.getConnection();

    public void inhabilitar() {

        txt_habitacion.setEnabled(false);
        txt_descuento.setEnabled(false);
        txt_importe.setEnabled(false);
        txt_item.setEnabled(false);
        dc_fechaSalida.setEnabled(false);
        dc_fechaIngreso.setEnabled(false);
        
        if (!IFrmReserva.btn_eliminar.isEnabled()==true) {
            btn_nuevo.setEnabled(false);
        }else{
            btn_nuevo.setEnabled(true);
        }
        
        btn_guardar.setEnabled(false);
        btn_eliminar.setEnabled(false);
        btn_cancelar.setEnabled(false);

        btn_buscarHabitacion.setEnabled(false);
        btn_factual_ing.setEnabled(false);
        btn_factual_sal.setEnabled(false);
        cbo_horas.setEnabled(false);
        cbo_horasSalida.setEnabled(false);
        cbo_minutos.setEnabled(false);
        cbo_minutosSalida.setEnabled(false);

        txt_habitacion.setText("");
        txt_descuento.setText("0.00");
        txt_importe.setText("0.00");
        txt_item.setText("");

    }

    public void habilitar() {

        txt_descuento.setEnabled(true);
        dc_fechaSalida.setEnabled(true);
        dc_fechaIngreso.setEnabled(true);

        btn_nuevo.setEnabled(false);
        if (!IFrmReserva.btn_eliminar.isEnabled()==true) {
            btn_guardar.setEnabled(false);
        }else{
            btn_guardar.setEnabled(true);
        }
        btn_eliminar.setEnabled(false);
        btn_cancelar.setEnabled(true);

        btn_buscarHabitacion.setEnabled(true);
        btn_factual_ing.setEnabled(true);
        btn_factual_sal.setEnabled(true);

        cbo_horas.setEnabled(true);
        cbo_horasSalida.setEnabled(true);
        cbo_minutos.setEnabled(true);
        cbo_minutosSalida.setEnabled(true);

    }

    void mostrarDetalles(String id_reserva, int item) {
        DetalleReservaDao detDao = new DetalleReservaDao();
        tbl_listado.setModel(detDao.mostrar(id_reserva, item));
        lbl_totalRegistros.setText("Nro. de Registros: " + Integer.toString(detDao.totalRegistros));
    }

    void hallarItemSiguiente() {
        DetalleReservaDao detDao = new DetalleReservaDao();
        int item = detDao.hallarItem(txt_idreserva.getText());
        txt_item.setText(String.valueOf(item));
    }

    void cargarEstado() {
        DetalleReservaDao detDao = new DetalleReservaDao();
        cbo_estado.setModel(detDao.mostrarEstadoInicio());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txt_idreserva = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txt_habitacion = new javax.swing.JTextField();
        btn_nuevo = new javax.swing.JButton();
        btn_guardar = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txt_item = new javax.swing.JTextField();
        dc_fechaIngreso = new com.toedter.calendar.JDateChooser();
        jLabel15 = new javax.swing.JLabel();
        btn_buscarHabitacion = new javax.swing.JButton();
        cbo_estado = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        dc_fechaSalida = new com.toedter.calendar.JDateChooser();
        btn_factual_sal = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        txt_importe = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txt_descuento = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        btn_factual_ing = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        cbo_horas = new javax.swing.JComboBox();
        cbo_minutos = new javax.swing.JComboBox();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        cbo_horasSalida = new javax.swing.JComboBox();
        cbo_minutosSalida = new javax.swing.JComboBox();
        jPanel2 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txt_buscar = new javax.swing.JTextField();
        btn_buscar = new javax.swing.JButton();
        btn_eliminar = new javax.swing.JButton();
        btn_salir = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_listado = new javax.swing.JTable();
        lbl_totalRegistros = new javax.swing.JLabel();
        btn_imprimir = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();

        jPanel5.setBackground(new java.awt.Color(122, 197, 205));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Registro del detalle de reserva");
        jLabel5.setToolTipText("");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel5)
        );

        jPanel3.setBackground(new java.awt.Color(79, 79, 79));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Detalle de Reserva");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(122, 197, 205));

        txt_idreserva.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_idreserva.setEnabled(false);
        txt_idreserva.setMaximumSize(new java.awt.Dimension(6, 6));
        txt_idreserva.setMinimumSize(new java.awt.Dimension(6, 6));
        txt_idreserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_idreservaActionPerformed(evt);
            }
        });
        txt_idreserva.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_idreservaKeyTyped(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Habitación");

        txt_habitacion.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_habitacion.setEnabled(false);
        txt_habitacion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_habitacionKeyTyped(evt);
            }
        });

        btn_nuevo.setBackground(new java.awt.Color(0, 0, 0));
        btn_nuevo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_nuevo.setForeground(new java.awt.Color(255, 255, 255));
        btn_nuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/plus.png"))); // NOI18N
        btn_nuevo.setText("Nuevo");
        btn_nuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_nuevoActionPerformed(evt);
            }
        });

        btn_guardar.setBackground(new java.awt.Color(0, 0, 0));
        btn_guardar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_guardar.setForeground(new java.awt.Color(255, 255, 255));
        btn_guardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ok1.png"))); // NOI18N
        btn_guardar.setText("Guardar");
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });

        btn_cancelar.setBackground(new java.awt.Color(0, 0, 0));
        btn_cancelar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_cancelar.setForeground(new java.awt.Color(255, 255, 255));
        btn_cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/cancelar.png"))); // NOI18N
        btn_cancelar.setText("Cancelar");
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Código Reserva");

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("Item (Detalle)");

        txt_item.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_item.setEnabled(false);
        txt_item.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_itemKeyTyped(evt);
            }
        });

        dc_fechaIngreso.setDateFormatString("dd/MM/yyyy");

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel15.setText("F. Ingreso");

        btn_buscarHabitacion.setBackground(new java.awt.Color(255, 51, 51));
        btn_buscarHabitacion.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_buscarHabitacion.setForeground(new java.awt.Color(255, 255, 255));
        btn_buscarHabitacion.setText("...");
        btn_buscarHabitacion.setToolTipText("");
        btn_buscarHabitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarHabitacionActionPerformed(evt);
            }
        });

        cbo_estado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cbo_estado.setEnabled(false);

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel16.setText("Estado");

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel17.setText("F. Salida");

        dc_fechaSalida.setDateFormatString("dd/MM/yyyy");

        btn_factual_sal.setBackground(new java.awt.Color(255, 51, 51));
        btn_factual_sal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_factual_sal.setForeground(new java.awt.Color(255, 255, 255));
        btn_factual_sal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/horaActual.png"))); // NOI18N
        btn_factual_sal.setToolTipText("");
        btn_factual_sal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_factual_salActionPerformed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel18.setText("Importe");

        txt_importe.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_importe.setText("0.00");
        txt_importe.setEnabled(false);
        txt_importe.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_importeKeyTyped(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel19.setText("Dscto.");

        txt_descuento.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_descuento.setText("0.00");
        txt_descuento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_descuentoKeyTyped(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel20.setText("(dd/MM/yyyy)");

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel21.setText("(dd/MM/yyyy)");

        btn_factual_ing.setBackground(new java.awt.Color(255, 51, 51));
        btn_factual_ing.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_factual_ing.setForeground(new java.awt.Color(255, 255, 255));
        btn_factual_ing.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/horaActual.png"))); // NOI18N
        btn_factual_ing.setToolTipText("");
        btn_factual_ing.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_factual_ingActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel14.setText("Hora");

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel22.setText("(hh:mm)");

        cbo_horas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24" }));

        cbo_minutos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", " " }));

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel23.setText("Hora");

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel24.setText("(hh:mm)");

        cbo_horasSalida.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24" }));

        cbo_minutosSalida.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", " " }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel2)
                        .addComponent(jLabel15))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(29, 29, 29)
                                .addComponent(jLabel22))
                            .addComponent(jLabel20)
                            .addComponent(jLabel17)
                            .addComponent(jLabel21))))
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(dc_fechaIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btn_factual_ing, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(dc_fechaSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btn_factual_sal, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGap(18, 18, 18))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(cbo_estado, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txt_item, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txt_descuento, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txt_importe, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(55, 55, 55)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txt_habitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_buscarHabitacion))
                            .addComponent(txt_idreserva, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(69, 69, 69))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(btn_nuevo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_guardar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_cancelar))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(130, 130, 130)
                .addComponent(cbo_horas, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbo_minutos, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel24)
                        .addGap(31, 31, 31)
                        .addComponent(cbo_horasSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbo_minutosSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel16)
                    .addComponent(jLabel12)
                    .addComponent(jLabel18)
                    .addComponent(jLabel19)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_idreserva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(7, 7, 7)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_habitacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(btn_buscarHabitacion))
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(dc_fechaIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_factual_ing))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbo_horas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbo_minutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(dc_fechaSalida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_factual_sal)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbo_horasSalida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbo_minutosSalida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel18)
                    .addComponent(txt_importe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel19)
                    .addComponent(txt_descuento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_item, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbo_estado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_nuevo)
                    .addComponent(btn_guardar)
                    .addComponent(btn_cancelar))
                .addGap(18, 18, 18))
        );

        jPanel2.setBackground(new java.awt.Color(235, 236, 228));

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setText("Item");

        txt_buscar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_buscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_buscarKeyTyped(evt);
            }
        });

        btn_buscar.setBackground(new java.awt.Color(0, 0, 0));
        btn_buscar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_buscar.setForeground(new java.awt.Color(255, 255, 255));
        btn_buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/buscar.png"))); // NOI18N
        btn_buscar.setText("Buscar");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });

        btn_eliminar.setBackground(new java.awt.Color(0, 0, 0));
        btn_eliminar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_eliminar.setForeground(new java.awt.Color(255, 255, 255));
        btn_eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/eliminar.png"))); // NOI18N
        btn_eliminar.setText("Inactivar");
        btn_eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_eliminarActionPerformed(evt);
            }
        });

        btn_salir.setBackground(new java.awt.Color(0, 0, 0));
        btn_salir.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_salir.setForeground(new java.awt.Color(255, 255, 255));
        btn_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/home.png"))); // NOI18N
        btn_salir.setText("Salir");
        btn_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salirActionPerformed(evt);
            }
        });

        tbl_listado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_listado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbl_listado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_listadoMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_listado);

        lbl_totalRegistros.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_totalRegistros.setText("Nro. de Registros");

        btn_imprimir.setBackground(new java.awt.Color(255, 51, 102));
        btn_imprimir.setForeground(new java.awt.Color(255, 255, 255));
        btn_imprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Print.png"))); // NOI18N
        btn_imprimir.setText("Imprimir");
        btn_imprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_imprimirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btn_imprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbl_totalRegistros, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btn_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btn_eliminar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btn_salir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 526, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 9, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txt_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_buscar)
                    .addComponent(btn_eliminar)
                    .addComponent(btn_salir))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_totalRegistros)
                    .addComponent(btn_imprimir))
                .addGap(14, 14, 14))
        );

        jPanel6.setBackground(new java.awt.Color(235, 236, 228));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Listado del detalle de reserva");
        jLabel6.setToolTipText("");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel6)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_idreservaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_idreservaKeyTyped
        int limite = 6;
        if (txt_idreserva.getText().length() == limite) {
            evt.consume();
        }
    }//GEN-LAST:event_txt_idreservaKeyTyped

    private void txt_habitacionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_habitacionKeyTyped

    }//GEN-LAST:event_txt_habitacionKeyTyped

    private void btn_nuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_nuevoActionPerformed
        habilitar();
        hallarItemSiguiente();
        cargarEstado();
    }//GEN-LAST:event_btn_nuevoActionPerformed

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed

        DetalleReservaDao detDao = new DetalleReservaDao();
        DetalleReserva det = new DetalleReserva();
        HabitacionDao habDao = new HabitacionDao();
        //validarCasillas();

        det.setId_reserva(txt_idreserva.getText());
        det.setItem(Integer.parseInt(txt_item.getText().trim()));
        det.setId_habitacion(habDao.capturarIdHabitacion(txt_habitacion.getText()));

        //FECHA INGRESO
        Calendar cal;
        int d, m, a, h, min;
        cal = dc_fechaIngreso.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        h = cal.get(Calendar.HOUR_OF_DAY);
        min = cal.get(Calendar.MINUTE);
        String fecha = cal.get(cal.DATE) + "/" + (cal.get(cal.MONTH) + 1) + "/" + cal.get(cal.YEAR);
        String hora = cbo_horas.getSelectedItem().toString() + ":" + cbo_minutos.getSelectedItem().toString();
        String fechaIngresoFinal = fecha + " " + hora;

        //FECHA SALIDA
        Calendar cal2;
        int d2, m2, a2, h2, min2;
        cal2 = dc_fechaSalida.getCalendar();
        d2 = cal2.get(Calendar.DAY_OF_MONTH);
        m2 = cal2.get(Calendar.MONTH);
        a2 = cal2.get(Calendar.YEAR) - 1900;
        h2 = cal2.get(Calendar.HOUR_OF_DAY);
        min2 = cal2.get(Calendar.MINUTE);
        fecha = cal2.get(cal2.DATE) + "/" + (cal2.get(cal2.MONTH) + 1) + "/" + cal2.get(cal2.YEAR);
        hora = cbo_horasSalida.getSelectedItem().toString() + ":" + cbo_minutosSalida.getSelectedItem().toString();
        String fechaSalidaFinal = fecha + " " + hora;

        det.setFecha_ingreso(fechaIngresoFinal);
        det.setFecha_salida(fechaSalidaFinal);
        det.setImporte(Double.parseDouble(txt_importe.getText()));
        det.setDescuento(Double.parseDouble(txt_descuento.getText()));

        EstadoDao e = new EstadoDao();
        String idEstado = e.capturarIdEstado(cbo_estado.getSelectedItem().toString());
        det.setId_estado(idEstado);

        //VALIDAMOS LA DISPONIBILIDAD -- EN CASO SE CAMBIEN FECHAS
        //Validamos la disponibilidad
        boolean check;
        String numHab = habDao.capturarIdHabitacion(txt_habitacion.getText());
        check = detDao.verificar_disponibilidad(fechaIngresoFinal, fechaSalidaFinal, numHab);

        if (a < 115 || a > 116 || m > 11 || m < 0 || d > 31 || d < 01) {
            JOptionPane.showMessageDialog(rootPane, "Debe ingresar una fecha de ingreso válida", "Aviso", WIDTH, null);
            return;
        } else {
            if (a2 < 115 || a2 > 116 || m2 > 11 || m2 < 0 || d2 > 31 || d2 < 01) {
                JOptionPane.showMessageDialog(rootPane, "Debe ingresar una fecha de salida válida", "Aviso", WIDTH, null);
                return;
            } else {
                if (check == true) {
                    if ("guardar".equals(accion)) {

                        int rpta = JOptionPane.showConfirmDialog(null, "¿Seguro que desea registrar el detalle?", "Registrar", JOptionPane.YES_NO_OPTION);
                        if (rpta == JOptionPane.YES_OPTION) {
                            boolean f = detDao.insertar(det);
                            if (f == true) {
                                JOptionPane.showMessageDialog(rootPane, "Detalle registrado con éxito", "Registrar", WIDTH, null);
                                mostrarDetalles(txt_idreserva.getText(), 0);
                                inhabilitar();
                            } else {
                                JOptionPane.showMessageDialog(rootPane, "No se pudo registrar el detalle... Verifique", "Registrar", WIDTH, null);
                            }

                        }
                    }

                    if ("editar".equals(accion)) {

                        int rpta = JOptionPane.showConfirmDialog(null, "¿Seguro que desea modificar el detalle?", "Modificar", JOptionPane.YES_NO_OPTION);
                        if (rpta == JOptionPane.YES_OPTION) {

                            detDao.editar(det);

                            JOptionPane.showMessageDialog(rootPane, "Detalle modificado con éxito", "Modificar", WIDTH, null);
                            mostrarDetalles(txt_idreserva.getText(), 0);
                            inhabilitar();
                            btn_guardar.setText("Guardar");
                            btn_eliminar.setEnabled(false);
                        }
                    }

                } else {
                    JOptionPane.showMessageDialog(rootPane, "La Habitación se encuentra ocupada para las fechas indicadas");
                }
            }
        }


    }//GEN-LAST:event_btn_guardarActionPerformed

    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        inhabilitar();
        accion = "guardar";
        btn_guardar.setText("Guardar");
    }//GEN-LAST:event_btn_cancelarActionPerformed

    private void txt_itemKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_itemKeyTyped

    }//GEN-LAST:event_txt_itemKeyTyped

    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed

        if (txt_buscar.getText().equalsIgnoreCase("")) {
            mostrarDetalles(txt_idreserva.getText(), 0);
        } else {
            mostrarDetalles(txt_idreserva.getText(), Integer.parseInt(txt_buscar.getText().trim()));
        }

    }//GEN-LAST:event_btn_buscarActionPerformed

    private void btn_eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_eliminarActionPerformed

        int confirmacion = JOptionPane.showConfirmDialog(rootPane, "¿Estás seguro que desea Inactivar la Habitación?", "Inactivar", JOptionPane.YES_NO_OPTION);
        if (confirmacion == 0) {
            DetalleReservaDao detDao = new DetalleReservaDao();
            DetalleReserva det = new DetalleReserva();

            det.setId_reserva(txt_idreserva.getText());
            det.setItem(Integer.parseInt(txt_item.getText().trim()));
            detDao.eliminar(det);

            JOptionPane.showConfirmDialog(rootPane, "Detalle inactivado con éxito", "Inactivar", JOptionPane.YES_NO_OPTION);
            mostrarDetalles(txt_idreserva.getText(), 0);
            inhabilitar();
        }

    }//GEN-LAST:event_btn_eliminarActionPerformed

    private void btn_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salirActionPerformed

        ReservaDao resDao = new ReservaDao();
        double totalPagar = resDao.totalPagarDetalle(txt_idreserva.getText());

        IFrmReserva.txt_totalPagar.setText(String.valueOf(totalPagar));
        //DecimalFormat df = new DecimalFormat("#.##");
        //df.format(0.912385);
        IFrmReserva.txt_confirma.setText(String.valueOf(totalPagar * 0.3));
        this.dispose();

    }//GEN-LAST:event_btn_salirActionPerformed

    private void tbl_listadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_listadoMouseClicked

        btn_guardar.setText("Editar");
        habilitar();
        accion = "editar";
        int fila = tbl_listado.rowAtPoint(evt.getPoint());

        txt_idreserva.setText(tbl_listado.getValueAt(fila, 0).toString());
        txt_item.setText(tbl_listado.getValueAt(fila, 1).toString());
        txt_habitacion.setText(tbl_listado.getValueAt(fila, 2).toString());

        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strFecha = tbl_listado.getValueAt(fila, 3).toString();
        Date fecha = null;
        //Recuperamos y cortamos la hora
        String h = strFecha.substring(11, 13);
        String m = strFecha.substring(14, 16);

        try {
            //FECHA CON EL FORMATO UTIL
            fecha = formato.parse(strFecha);
        } catch (ParseException ex) {
            Logger.getLogger(IFrmReserva.class.getName()).log(Level.SEVERE, null, ex);
        }

        dc_fechaIngreso.setDate(fecha);
        cbo_horas.setSelectedItem(h);
        cbo_minutos.setSelectedItem(m);

        strFecha = tbl_listado.getValueAt(fila, 4).toString();
        fecha = null;
        h = strFecha.substring(11, 13);
        m = strFecha.substring(14, 16);

        try {
            //FECHA CON EL FORMATO UTIL
            fecha = formato.parse(strFecha);
        } catch (ParseException ex) {
            Logger.getLogger(IFrmReserva.class.getName()).log(Level.SEVERE, null, ex);
        }

        dc_fechaSalida.setDate(fecha);
        cbo_horasSalida.setSelectedItem(h);
        cbo_minutosSalida.setSelectedItem(m);

        txt_importe.setText(tbl_listado.getValueAt(fila, 5).toString());
        txt_descuento.setText(tbl_listado.getValueAt(fila, 6).toString());
        cbo_estado.setSelectedItem(tbl_listado.getValueAt(fila, 7).toString());

        btn_eliminar.setEnabled(true);

    }//GEN-LAST:event_tbl_listadoMouseClicked

    private void txt_importeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_importeKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_importeKeyTyped

    private void txt_descuentoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_descuentoKeyTyped

        char c = evt.getKeyChar();
        int limite = 8;

        if (Character.isLetter(c)) {
            getToolkit().beep();

            evt.consume();

            JOptionPane.showMessageDialog(rootPane, "Ingresa solo Numeros", "Mensaje", WIDTH, null);
            //Error.setText("Ingresa Solo Numeros");
        } else {
            if (txt_descuento.getText().length() == limite) {
                evt.consume();
            }
        }

    }//GEN-LAST:event_txt_descuentoKeyTyped

    private void txt_idreservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_idreservaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_idreservaActionPerformed

    private void btn_factual_salActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_factual_salActionPerformed
        Calendar fecha = Calendar.getInstance();
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);

        cbo_horasSalida.setSelectedItem(String.valueOf(hora));
        cbo_minutosSalida.setSelectedItem(String.valueOf(minuto));

        dc_fechaSalida.setCalendar(fecha);
    }//GEN-LAST:event_btn_factual_salActionPerformed

    private void btn_buscarHabitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarHabitacionActionPerformed

        IFrmHabitacionVista vHab = new IFrmHabitacionVista();
        escritorio.add(vHab);
        vHab.toFront();
        vHab.setVisible(true);

    }//GEN-LAST:event_btn_buscarHabitacionActionPerformed

    private void btn_factual_ingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_factual_ingActionPerformed

        Calendar fecha = Calendar.getInstance();
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);

        cbo_horas.setSelectedItem(String.valueOf(hora));
        cbo_minutos.setSelectedItem(String.valueOf(minuto));

        dc_fechaIngreso.setCalendar(fecha);
    }//GEN-LAST:event_btn_factual_ingActionPerformed

    private void txt_buscarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_buscarKeyTyped

        char c = evt.getKeyChar();
        int limite = 3;

        if (Character.isLetter(c)) {
            getToolkit().beep();

            evt.consume();

            JOptionPane.showMessageDialog(rootPane, "Ingresa solo Numeros", "Mensaje", WIDTH, null);
            //Error.setText("Ingresa Solo Numeros");
        } else {
            if (txt_buscar.getText().length() == limite) {
                evt.consume();
            }
        }

    }//GEN-LAST:event_txt_buscarKeyTyped

    private void btn_imprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_imprimirActionPerformed

        try {
            String ruta = "src\\Reportes\\rDetalle.jrxml";
            JasperReport rep = JasperCompileManager.compileReport(ruta);
            
            //Se crea un objeto HashMap
            HashMap<String, Object> parametros = new HashMap<String, Object>();
            parametros.clear();
            //el nombre que se dio al parametro en JasperReport fue "p1", y se debe llamar desde Java con
            //ese mismo nombre, a su lado se pasa el valor del parametro
            parametros.put("idReserva", txt_idReserva.getText());

            JasperPrint reporte = JasperFillManager.fillReport(rep, parametros, cn);
            JasperViewer visor = new JasperViewer(reporte, false);
            visor.setTitle("Reserva");
            visor.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPane, "Error" + e.toString());
        }
    }//GEN-LAST:event_btn_imprimirActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_buscar;
    public static javax.swing.JButton btn_buscarHabitacion;
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_eliminar;
    private javax.swing.JButton btn_factual_ing;
    private javax.swing.JButton btn_factual_sal;
    private javax.swing.JButton btn_guardar;
    private javax.swing.JButton btn_imprimir;
    private javax.swing.JButton btn_nuevo;
    private javax.swing.JButton btn_salir;
    private javax.swing.JComboBox cbo_estado;
    public static javax.swing.JComboBox cbo_horas;
    public static javax.swing.JComboBox cbo_horasSalida;
    public static javax.swing.JComboBox cbo_minutos;
    public static javax.swing.JComboBox cbo_minutosSalida;
    public static com.toedter.calendar.JDateChooser dc_fechaIngreso;
    public static com.toedter.calendar.JDateChooser dc_fechaSalida;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lbl_totalRegistros;
    private javax.swing.JTable tbl_listado;
    private javax.swing.JTextField txt_buscar;
    public static javax.swing.JTextField txt_descuento;
    public static javax.swing.JTextField txt_habitacion;
    private javax.swing.JTextField txt_idreserva;
    public static javax.swing.JTextField txt_importe;
    private javax.swing.JTextField txt_item;
    // End of variables declaration//GEN-END:variables
}
