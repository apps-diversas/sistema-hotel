package Presentacion;

import Dao.DetalleReservaDao;
import Dao.HabitacionDao;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class IFrmHabitacionVista extends javax.swing.JInternalFrame {

    public IFrmHabitacionVista() {
        initComponents();
        inhabilitar();
        mostrarTipoHabitacion();
        ListenerTipoHab();
    }
    public double importe;
    public String fechaIngresoFinal;
    public String fechaSalidaFinal;

    public String hi;
    public String mi;
    public String hs;
    public String ms;

    java.util.Date fechaIng = null;
    java.util.Date fechaSal = null;

    void inhabilitar() {
        txt_precio.setEnabled(false);
        cbo_habitacion.setEnabled(false);
        btn_buscar.setEnabled(false);
        dc_fechaIngreso.setEnabled(false);
        dc_fechaSalida.setEnabled(false);
        btn_añadir.setEnabled(false);
        cbo_horas.setEnabled(false);
        cbo_minutos.setEnabled(false);
        cbo_horasSalida.setEnabled(false);
        cbo_minutosSalida.setEnabled(false);
    }

    void habilitar() {
        //txt_precio.setEnabled(true);
        cbo_habitacion.setEnabled(true);
        btn_buscar.setEnabled(true);
    }

    void mostrarTipoHabitacion() {
        HabitacionDao habDao = new HabitacionDao();
        cbo_tipoHab.setModel(habDao.mostrarTipoHabitacion());
    }

    void ListenerTipoHab() {

        cbo_tipoHab.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    // los nuevos registros son agregados al MODEL del JCombo Habitacion
                    Object item = e.getItem();
                    HabitacionDao habDao = new HabitacionDao();
                    String idTipo = habDao.capturarIdTipoHabitacion(item.toString());
                    cbo_habitacion.setModel(habDao.mostrarHabitacionxTipo(idTipo));
                    //cbo_habitacion.setModel( access.animales( item.toString() ) );
                    if (cbo_habitacion.getSelectedItem() != null) {
                        habilitar();
                        txt_precio.setText(String.valueOf(habDao.mostrarPrecioxTipoHab(idTipo)));
                    } else {
                    }
                }
            }
        });

    }

    boolean validarFechas() {
        boolean f = false;

        if (dc_fechaIngreso.getDate() != null) {
            f = true;
        } else {
            JOptionPane.showMessageDialog(rootPane, "Ingrese una Fecha de Ingreso válida");
            f = false;
        }

        if (dc_fechaIngreso.getDate() != null) {
            f = true;
        } else {

            JOptionPane.showMessageDialog(rootPane, "Ingrese una Fecha de Salida válida");
            f = false;
        }

        if (dc_fechaIngreso.getDate().after(dc_fechaSalida.getDate())) {
            JOptionPane.showMessageDialog(rootPane, "Fecha de Salida tiene que ser mayor a la Fecha de Ingreso");
            f = false;
        }
        
        Calendar cal;
        cal = dc_fechaIngreso.getCalendar();
        String fechaIngreso = cal.get(cal.DATE) + "/" + (cal.get(cal.MONTH) + 1) + "/" + cal.get(cal.YEAR);
        
        Calendar cal2;
        cal2 = dc_fechaSalida.getCalendar();
        String fechaSalida = cal2.get(cal2.DATE) + "/" + (cal2.get(cal2.MONTH) + 1) + "/" + cal2.get(cal2.YEAR);
        
   
        if (fechaIngreso.equalsIgnoreCase(fechaSalida)){
            if ((cbo_horas.getSelectedIndex()==cbo_horasSalida.getSelectedIndex())||(cbo_horasSalida.getSelectedIndex() - cbo_horas.getSelectedIndex()) < 5) {
                JOptionPane.showMessageDialog(rootPane, "Las Fechas deben tener una diferencia mínima de 5 horas");
            f = false;
            }
        }

        return f;
    }

    void calcularImporte() {
        //Calculando Cantidad de Días
        long fechaInicialMs = fechaIng.getTime();
        long fechaFinalMs = fechaSal.getTime();
        long diferencia = fechaFinalMs - fechaInicialMs;
        double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
        //Obteniendo el precio
        double precio = Double.parseDouble(txt_precio.getText());

        //Calculando Importe
        importe = precio * dias;
    }

    void seteoValoresDetalle() {

        IFrmDetalleReserva.txt_habitacion.setText(cbo_habitacion.getSelectedItem().toString());

        IFrmDetalleReserva.cbo_horas.setSelectedItem(hi);
        IFrmDetalleReserva.cbo_horasSalida.setSelectedItem(hs);

        IFrmDetalleReserva.cbo_minutos.setSelectedItem(mi);
        IFrmDetalleReserva.cbo_minutosSalida.setSelectedItem(ms);

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        String strFechaIngreso = fechaIngresoFinal;
        String strFechaSalida = fechaSalidaFinal;

        try {
            //FECHA CON EL FORMATO UTIL
            fechaIng = formato.parse(strFechaIngreso);
        } catch (ParseException ex) {
            Logger.getLogger(IFrmReserva.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            //FECHA CON EL FORMATO UTIL
            fechaSal = formato.parse(strFechaSalida);
        } catch (ParseException ex) {
            Logger.getLogger(IFrmReserva.class.getName()).log(Level.SEVERE, null, ex);
        }

        calcularImporte();
        IFrmDetalleReserva.dc_fechaIngreso.setDate(fechaIng);
        IFrmDetalleReserva.dc_fechaSalida.setDate(fechaSal);
        IFrmDetalleReserva.txt_importe.setText(String.valueOf(importe));

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tbl_listadoHabs = new javax.swing.JTable();
        btn_buscar = new javax.swing.JButton();
        lbl_totalRegistros = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        cbo_tipoHab = new javax.swing.JComboBox();
        cbo_habitacion = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        txt_precio = new javax.swing.JTextField();
        dc_fechaIngreso = new com.toedter.calendar.JDateChooser();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        dc_fechaSalida = new com.toedter.calendar.JDateChooser();
        btn_añadir = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btn_salir = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        cbo_horas = new javax.swing.JComboBox();
        cbo_minutos = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        cbo_horasSalida = new javax.swing.JComboBox();
        cbo_minutosSalida = new javax.swing.JComboBox();
        btn_factual_ing = new javax.swing.JButton();
        btn_factual_ing1 = new javax.swing.JButton();

        jPanel5.setBackground(new java.awt.Color(235, 236, 228));
        jPanel5.setBorder(null);

        tbl_listadoHabs.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_listadoHabs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbl_listadoHabs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_listadoHabsMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tbl_listadoHabsMousePressed(evt);
            }
        });
        jScrollPane6.setViewportView(tbl_listadoHabs);

        btn_buscar.setBackground(new java.awt.Color(0, 0, 0));
        btn_buscar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_buscar.setForeground(new java.awt.Color(255, 255, 255));
        btn_buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/buscar.png"))); // NOI18N
        btn_buscar.setText("Buscar Reservas");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });

        lbl_totalRegistros.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_totalRegistros.setText("Nro. de Registros");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Listado de reservaciones por Habitacion");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("Nro. Habitación");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Tipo Habitación");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setText("Precio Diario");

        dc_fechaIngreso.setDateFormatString("dd/MM/yyyy");
        dc_fechaIngreso.setMinSelectableDate(new java.util.Date(-62135668726000L));

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("F. Ingreso");

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel13.setText("F. Salida");

        dc_fechaSalida.setDateFormatString("dd/MM/yyyy");

        btn_añadir.setBackground(new java.awt.Color(0, 0, 0));
        btn_añadir.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_añadir.setForeground(new java.awt.Color(255, 255, 255));
        btn_añadir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ok1.png"))); // NOI18N
        btn_añadir.setText("Añadir al Detalle de Reserva");
        btn_añadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_añadirActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Listado de Reservaciones a la fecha por Habitacion");

        btn_salir.setBackground(new java.awt.Color(0, 0, 0));
        btn_salir.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_salir.setForeground(new java.awt.Color(255, 255, 255));
        btn_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/home.png"))); // NOI18N
        btn_salir.setText("Salir");
        btn_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salirActionPerformed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel17.setText("(dd/MM/yyyy)");

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel18.setText("(dd/MM/yyyy)");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel14.setText("Hora");

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel15.setText("(hh:mm:ss)");

        cbo_horas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24" }));

        cbo_minutos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", " " }));

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel16.setText("Hora");

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel19.setText("(hh:mm:ss)");

        cbo_horasSalida.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24" }));

        cbo_minutosSalida.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", " " }));

        btn_factual_ing.setBackground(new java.awt.Color(255, 51, 51));
        btn_factual_ing.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_factual_ing.setForeground(new java.awt.Color(255, 255, 255));
        btn_factual_ing.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/horaActual.png"))); // NOI18N
        btn_factual_ing.setToolTipText("");
        btn_factual_ing.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_factual_ingActionPerformed(evt);
            }
        });

        btn_factual_ing1.setBackground(new java.awt.Color(255, 51, 51));
        btn_factual_ing1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_factual_ing1.setForeground(new java.awt.Color(255, 255, 255));
        btn_factual_ing1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/horaActual.png"))); // NOI18N
        btn_factual_ing1.setToolTipText("");
        btn_factual_ing1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_factual_ing1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btn_salir, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 834, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                .addComponent(lbl_totalRegistros, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(9, 9, 9))))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addComponent(jLabel10)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cbo_tipoHab, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addComponent(jLabel9)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cbo_habitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel14)
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addGap(29, 29, 29)
                                        .addComponent(jLabel15)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cbo_horas, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cbo_minutos, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel5Layout.createSequentialGroup()
                                                .addGap(17, 17, 17)
                                                .addComponent(btn_factual_ing, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel18)
                                                    .addComponent(jLabel13))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(dc_fechaSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel5Layout.createSequentialGroup()
                                                .addGap(58, 58, 58)
                                                .addComponent(jLabel16)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel19)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(cbo_horasSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(cbo_minutosSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btn_factual_ing1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(btn_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel5Layout.createSequentialGroup()
                                                .addComponent(jLabel11)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txt_precio, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel17)
                                    .addComponent(jLabel12))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(dc_fechaIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_añadir, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(9, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cbo_tipoHab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(txt_precio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(cbo_habitacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_buscar))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(dc_fechaIngreso, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_factual_ing, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbo_minutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbo_horas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btn_añadir)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(dc_fechaSalida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                    .addComponent(jLabel13)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(btn_factual_ing1)))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbo_minutosSalida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbo_horasSalida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(11, 11, 11)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(btn_salir))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_totalRegistros)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tbl_listadoHabsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_listadoHabsMouseClicked

    }//GEN-LAST:event_tbl_listadoHabsMouseClicked

    private void tbl_listadoHabsMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_listadoHabsMousePressed

    }//GEN-LAST:event_tbl_listadoHabsMousePressed

    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed

        String numerohab = cbo_habitacion.getSelectedItem().toString();
        HabitacionDao habDao = new HabitacionDao();

        tbl_listadoHabs.setModel(habDao.mostrarVista(numerohab));
        lbl_totalRegistros.setText("Nro. de Registros: " + Integer.toString(habDao.totalRegistros));

        if (habDao.totalRegistros == 0) {
            JOptionPane.showMessageDialog(rootPane, "No existen reservas para esta habitación");
        } else {
        }

        dc_fechaIngreso.setEnabled(true);
        dc_fechaSalida.setEnabled(true);
        btn_añadir.setEnabled(true);
        cbo_horas.setEnabled(true);
        cbo_minutos.setEnabled(true);
        cbo_horasSalida.setEnabled(true);
        cbo_minutosSalida.setEnabled(true);

    }//GEN-LAST:event_btn_buscarActionPerformed

    private void btn_añadirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_añadirActionPerformed

        boolean f = validarFechas();

        if (f == true) {
            Calendar cal;
            cal = dc_fechaIngreso.getCalendar();

            Calendar cal2;
            cal2 = dc_fechaSalida.getCalendar();

            String fechaIngreso = cal.get(cal.DATE) + "/" + (cal.get(cal.MONTH) + 1) + "/" + cal.get(cal.YEAR);
            hi = cbo_horas.getSelectedItem().toString();
            mi = cbo_minutos.getSelectedItem().toString();

            String fechaSalida = cal2.get(cal2.DATE) + "/" + (cal2.get(cal2.MONTH) + 1) + "/" + cal2.get(cal2.YEAR);
            hs = cbo_horasSalida.getSelectedItem().toString();
            ms = cbo_minutosSalida.getSelectedItem().toString();

            fechaIngresoFinal = fechaIngreso;
            fechaSalidaFinal = fechaSalida;

            //Validamos la disponibilidad
            DetalleReservaDao detDao = new DetalleReservaDao();
            HabitacionDao habDao = new HabitacionDao();
            boolean check;
            String numHab = habDao.capturarIdHabitacion(cbo_habitacion.getSelectedItem().toString());
            check = detDao.verificar_disponibilidad(fechaIngresoFinal + " " + hi + ":" + mi, fechaSalidaFinal + " " + hs + ":" + ms, numHab);

            if (check == true) {
                seteoValoresDetalle();
                IFrmDetalleReserva.btn_buscarHabitacion.setEnabled(false);
                IFrmDetalleReserva.txt_descuento.requestFocus();
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(rootPane, "La Habitación se encuentra ocupada para las fechas indicadas");
            }
        }


    }//GEN-LAST:event_btn_añadirActionPerformed

    private void btn_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btn_salirActionPerformed

    private void btn_factual_ingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_factual_ingActionPerformed

        Calendar fecha = Calendar.getInstance();
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);

        cbo_horas.setSelectedItem(String.valueOf(hora));
        cbo_minutos.setSelectedItem(String.valueOf(minuto));

        dc_fechaIngreso.setCalendar(fecha);
    }//GEN-LAST:event_btn_factual_ingActionPerformed

    private void btn_factual_ing1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_factual_ing1ActionPerformed

        Calendar fecha = Calendar.getInstance();
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);

        cbo_horasSalida.setSelectedItem(String.valueOf(hora));
        cbo_minutosSalida.setSelectedItem(String.valueOf(minuto));

        dc_fechaSalida.setCalendar(fecha);

    }//GEN-LAST:event_btn_factual_ing1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_añadir;
    private javax.swing.JButton btn_buscar;
    private javax.swing.JButton btn_factual_ing;
    private javax.swing.JButton btn_factual_ing1;
    private javax.swing.JButton btn_salir;
    private javax.swing.JComboBox cbo_habitacion;
    private javax.swing.JComboBox cbo_horas;
    private javax.swing.JComboBox cbo_horasSalida;
    private javax.swing.JComboBox cbo_minutos;
    private javax.swing.JComboBox cbo_minutosSalida;
    private javax.swing.JComboBox cbo_tipoHab;
    private com.toedter.calendar.JDateChooser dc_fechaIngreso;
    private com.toedter.calendar.JDateChooser dc_fechaSalida;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JLabel lbl_totalRegistros;
    private javax.swing.JTable tbl_listadoHabs;
    private javax.swing.JTextField txt_precio;
    // End of variables declaration//GEN-END:variables
}
