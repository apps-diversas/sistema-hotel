package Presentacion;

import Dao.EstadoDao;
import Dao.PagoDao;
import Dao.TipoComprobanteDao;
import Dao.TipoPagoDao;
import Datos.Pago;
import static Presentacion.FrmInicio.escritorio;
import static Presentacion.IFrmReserva.txt_idReserva;
import Utilidades.Conexion;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import static java.awt.image.ImageObserver.WIDTH;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

public class IFrmPago extends javax.swing.JInternalFrame {

    public IFrmPago() {
        initComponents();
        cargarEstadoInicio();
        cargarTipoComprobante();
        cargarTipoPago();
        inhabilitar();
        ListenerTipoPago();
        ListenerTipoC();
    }
    
    
    Connection cn = Conexion.getConnection();

    public void cargarEstadoInicio() {
        PagoDao pagoDao = new PagoDao();
        cbo_estado.setModel(pagoDao.mostrarEstadoInicio());
    }

    public void cargarTipoPago() {
        PagoDao pagoDao = new PagoDao();
        cbo_tipoPago.setModel(pagoDao.mostrarTipoPago());
    }

    public void cargarTipoComprobante() {
        PagoDao pagoDao = new PagoDao();
        cbo_tipoComprobante.setModel(pagoDao.mostrarTipoComprobante());
    }

    void ListenerTipoPago() {

        cbo_tipoPago.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    // los nuevos registros son agregados al MODEL del JCombo Habitacion
                    Object item = e.getItem();
                    TipoPagoDao tipoPagoDao = new TipoPagoDao();

                    int correlativo = tipoPagoDao.capturarCorrelativo(tipoPagoDao.capturarIdEstado(item.toString()));
                    txt_nroPago.setText(String.valueOf(correlativo));
                    if (correlativo > 0) {
                        txt_nroPago.setText(String.valueOf(correlativo));
                    } else {
                        txt_nroPago.setText("1");
                    }
                }
            }
        });

    }

    void ListenerTipoC() {

        cbo_tipoComprobante.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    // los nuevos registros son agregados al MODEL del JCombo Habitacion
                    Object item = e.getItem();
                    TipoComprobanteDao tipoDao = new TipoComprobanteDao();

                    int correlativo = tipoDao.capturarCorrelativo(tipoDao.capturarIdEstado(item.toString()));
                    if (correlativo > 0) {
                        txt_nroComprob.setText(String.valueOf(correlativo));
                    } else {
                        txt_nroComprob.setText("1");
                    }
                }
            }
        });

    }

    void inhabilitar() {

        txt_nroPago.setEnabled(false);
        txt_idReserva.setEnabled(false);
        txt_nroComprob.setEnabled(false);
        txt_subtotal.setEnabled(false);
        txt_total.setEnabled(false);
        txt_igv.setEnabled(false);
        chk_confirma.setEnabled(false);

        btn_generarComp.setEnabled(false);
        btn_nuevo.setEnabled(true);
        btn_cancelar.setEnabled(false);

        btn_guardar.setEnabled(false);

        cbo_tipoComprobante.setEnabled(true);
        cbo_tipoPago.setEnabled(true);
        cbo_estado.setEnabled(false);
    }

    void habilitar() {
        btn_guardar.setEnabled(true);
        btn_cancelar.setEnabled(true);
        btn_nuevo.setEnabled(false);
    }

    void inhabilitar_cancelar() {
        txt_total.setText("");
        txt_subtotal.setText("");
        txt_igv.setText("");
        txt_nroPago.setText("");
        txt_nroComprob.setText("");
        inhabilitar();
        btn_consultar.setEnabled(true);
    }

    public void validarCasillas() {
        if (txt_nroPago.getText().length() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Debes ingresar un Nro de Pago válido");
            cbo_tipoPago.requestFocus();
            return;
        }
        if (txt_nroComprob.getText().length() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Debe ingresar un Nro de Comprobante válido");
            cbo_tipoComprobante.requestFocus();
            return;
        }
        if (txt_subtotal.getText().length() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Debe ingresar un Subtotal válido");
            txt_idPago.requestFocus();
            return;
        }
        if (txt_igv.getText().length() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Debe ingresar un IGV válido");
            txt_idPago.requestFocus();
            return;
        }
        if (txt_total.getText().length() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Debe ingresar un Total válido");
            txt_idPago.requestFocus();
            return;
        }
        
        Calendar cal;
        int a;
        cal = dc_fecha_emi.getCalendar();
        a = cal.get(Calendar.YEAR);
        if (a <= 0) {
            JOptionPane.showMessageDialog(rootPane, "Debe ingresar una Fecha válida");
            btn_fechaActual.requestFocus();
            return;
        }
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txt_idPago = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txt_nroPago = new javax.swing.JTextField();
        btn_nuevo = new javax.swing.JButton();
        btn_guardar = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txt_subtotal = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txt_igv = new javax.swing.JTextField();
        dc_fecha_emi = new com.toedter.calendar.JDateChooser();
        jLabel15 = new javax.swing.JLabel();
        btn_consultar = new javax.swing.JButton();
        cbo_estado = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txt_idReserva = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        btn_consultarPagos = new javax.swing.JButton();
        btn_salir = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        cbo_tipoPago = new javax.swing.JComboBox();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        cbo_tipoComprobante = new javax.swing.JComboBox();
        txt_total = new javax.swing.JTextField();
        btn_generarComp = new javax.swing.JButton();
        btn_fechaActual = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txt_nroComprob = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        chk_confirma = new javax.swing.JCheckBox();

        jPanel5.setBackground(new java.awt.Color(235, 236, 228));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Registro de pagos");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel5)
        );

        jPanel3.setBackground(new java.awt.Color(79, 79, 79));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Pago");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(235, 236, 228));

        txt_idPago.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_idPago.setMaximumSize(new java.awt.Dimension(6, 6));
        txt_idPago.setMinimumSize(new java.awt.Dimension(6, 6));
        txt_idPago.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_idPagoKeyTyped(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Nro. Pago");

        txt_nroPago.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_nroPago.setEnabled(false);
        txt_nroPago.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_nroPagoKeyTyped(evt);
            }
        });

        btn_nuevo.setBackground(new java.awt.Color(0, 0, 0));
        btn_nuevo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_nuevo.setForeground(new java.awt.Color(255, 255, 255));
        btn_nuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/plus.png"))); // NOI18N
        btn_nuevo.setText("Nuevo");
        btn_nuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_nuevoActionPerformed(evt);
            }
        });

        btn_guardar.setBackground(new java.awt.Color(0, 0, 0));
        btn_guardar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_guardar.setForeground(new java.awt.Color(255, 255, 255));
        btn_guardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ok1.png"))); // NOI18N
        btn_guardar.setText("Guardar");
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });

        btn_cancelar.setBackground(new java.awt.Color(0, 0, 0));
        btn_cancelar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_cancelar.setForeground(new java.awt.Color(255, 255, 255));
        btn_cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/cancelar.png"))); // NOI18N
        btn_cancelar.setText("Cancelar");
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Código Pago");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setText("Sub Total");

        txt_subtotal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_subtotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_subtotalKeyTyped(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("IGV (18%)");

        txt_igv.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_igv.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_igvKeyTyped(evt);
            }
        });

        dc_fecha_emi.setDateFormatString("dd/MM/yyyy");
        dc_fecha_emi.setEnabled(false);

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel15.setText("F. Emisión");

        btn_consultar.setBackground(new java.awt.Color(255, 51, 51));
        btn_consultar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_consultar.setForeground(new java.awt.Color(255, 255, 255));
        btn_consultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/buscar.png"))); // NOI18N
        btn_consultar.setText("Consultar");
        btn_consultar.setToolTipText("");
        btn_consultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_consultarActionPerformed(evt);
            }
        });

        cbo_estado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel16.setText("Estado");

        jPanel2.setBackground(new java.awt.Color(95, 158, 160));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.gray, java.awt.Color.gray));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Reserva");

        txt_idReserva.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_idReserva.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_idReservaKeyTyped(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Codigo Reserva");

        btn_consultarPagos.setBackground(new java.awt.Color(254, 186, 84));
        btn_consultarPagos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_consultarPagos.setForeground(new java.awt.Color(51, 51, 51));
        btn_consultarPagos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/buscar.png"))); // NOI18N
        btn_consultarPagos.setText("Consultar Pagos");
        btn_consultarPagos.setToolTipText("");
        btn_consultarPagos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_consultarPagosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btn_consultarPagos, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_idReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txt_idReserva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_consultarPagos)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        btn_salir.setBackground(new java.awt.Color(0, 0, 0));
        btn_salir.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_salir.setForeground(new java.awt.Color(255, 255, 255));
        btn_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/home.png"))); // NOI18N
        btn_salir.setText("Salir");
        btn_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salirActionPerformed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel18.setText("Total");

        cbo_tipoPago.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel19.setText("Tipo Pago");

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel20.setText("Comprobante");

        cbo_tipoComprobante.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        txt_total.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_total.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_totalKeyTyped(evt);
            }
        });

        btn_generarComp.setBackground(new java.awt.Color(0, 0, 0));
        btn_generarComp.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_generarComp.setForeground(new java.awt.Color(255, 255, 255));
        btn_generarComp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/detalleImprimir.png"))); // NOI18N
        btn_generarComp.setText("Generar Comprobante");
        btn_generarComp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_generarCompActionPerformed(evt);
            }
        });

        btn_fechaActual.setBackground(new java.awt.Color(255, 51, 51));
        btn_fechaActual.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_fechaActual.setForeground(new java.awt.Color(255, 255, 255));
        btn_fechaActual.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/horaActual.png"))); // NOI18N
        btn_fechaActual.setToolTipText("");
        btn_fechaActual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_fechaActualActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Nro. Comp.");

        txt_nroComprob.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_nroComprob.setEnabled(false);
        txt_nroComprob.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_nroComprobKeyTyped(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel21.setText("Confirmación");

        chk_confirma.setText("(30% del Total)");
        chk_confirma.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                chk_confirmaMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chk_confirma)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel20)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel15)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel18)
                                    .addComponent(jLabel16)
                                    .addComponent(jLabel19))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_nroPago, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_nroComprob, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_igv, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_total, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cbo_estado, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cbo_tipoComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(dc_fecha_emi, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btn_fechaActual, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(cbo_tipoPago, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txt_idPago, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btn_consultar)))
                                .addGap(10, 10, 10))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txt_subtotal, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(113, 113, 113)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_generarComp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btn_nuevo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btn_cancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btn_guardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btn_salir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(18, 18, 18))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_nuevo)
                            .addComponent(btn_guardar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_cancelar)
                            .addComponent(btn_salir)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txt_idPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_consultar)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbo_tipoPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_nroPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbo_tipoComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txt_nroComprob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel15))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(11, 11, 11)
                                        .addComponent(dc_fecha_emi, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btn_fechaActual)))))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_subtotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_igv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_total, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(btn_generarComp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chk_confirma))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbo_estado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16))
                .addGap(25, 25, 25))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(37, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(17, 17, 17))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_idPagoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_idPagoKeyTyped
        int limite = 6;
        if (txt_idPago.getText().length() == limite) {
            evt.consume();
        }
    }//GEN-LAST:event_txt_idPagoKeyTyped

    private void txt_nroPagoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nroPagoKeyTyped
        //
        //        char c = evt.getKeyChar();
        //        int limite = 8;
        //
        //        if (Character.isLetter(c)) {
        //            getToolkit().beep();
        //
        //            evt.consume();
        //
        //            JOptionPane.showMessageDialog(rootPane, "Ingresa solo Numeros", "Mensaje", WIDTH, null);
        //            //Error.setText("Ingresa Solo Numeros");
        //        } else {
        //            if (txt_Dni.getText().length() == limite) {
        //                evt.consume();
        //            }
        //        }
    }//GEN-LAST:event_txt_nroPagoKeyTyped

    private void btn_nuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_nuevoActionPerformed
        habilitar();
        btn_consultar.setEnabled(false);
    }//GEN-LAST:event_btn_nuevoActionPerformed

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed

        validarCasillas();

        Pago pago = new Pago();
        PagoDao pagoDao = new PagoDao();
        TipoPagoDao tipoDao = new TipoPagoDao();
        TipoComprobanteDao tipoCDao = new TipoComprobanteDao();
        EstadoDao estadoDao = new EstadoDao();

        Calendar cal;
        int d, m, a, h, min;
        cal = dc_fecha_emi.getCalendar();

        String fechaEmision = cal.get(cal.DATE) + "/" + (cal.get(cal.MONTH) + 1) + "/" + cal.get(cal.YEAR);

        pago.setIdPago(txt_idPago.getText());
        pago.setIdReserva(txt_idReserva.getText());
        pago.setIdTipoPago(tipoDao.capturarIdEstado(cbo_tipoPago.getSelectedItem().toString()));
        pago.setIdTipoComp(tipoCDao.capturarIdEstado(cbo_tipoComprobante.getSelectedItem().toString()));
        pago.setNroPago(Integer.parseInt(txt_nroPago.getText()));
        pago.setNroComprobante(Integer.parseInt(txt_nroComprob.getText()));
        pago.setFechaEmision(fechaEmision);

        pago.setTotal(Double.parseDouble(txt_total.getText()));
        pago.setIgv(Double.parseDouble(txt_igv.getText()));
        pago.setSubTotal(Double.parseDouble(txt_subtotal.getText()));
        pago.setIdEstado(estadoDao.capturarIdEstado(cbo_estado.getSelectedItem().toString()));

        String flag = "0";
        if (chk_confirma.isSelected() == true) {
            flag = "1";
        }
        pago.setFlag_confirma(flag);

        int rpta = JOptionPane.showConfirmDialog(null, "¿Seguro que desea registrar el pago?", "Registrar", JOptionPane.YES_NO_OPTION);
        if (rpta == JOptionPane.YES_OPTION) {
            boolean f = pagoDao.insertar(pago);
            if (f == true) {
                JOptionPane.showMessageDialog(rootPane, "Pago registrado con éxito", "Registrar", WIDTH, null);
                inhabilitar_cancelar();
                btn_generarComp.setEnabled(true);
            } else {
                JOptionPane.showMessageDialog(rootPane, "No se pudo registrar el pago... Verifique", "Registrar", WIDTH, null);
            }
        }

    }//GEN-LAST:event_btn_guardarActionPerformed

    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        inhabilitar_cancelar();
    }//GEN-LAST:event_btn_cancelarActionPerformed

    private void txt_subtotalKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_subtotalKeyTyped

        //        char c = evt.getKeyChar();
        //        int limite = 40;
        //
        //        if (Character.isDigit(c)) {
        //            getToolkit().beep();
        //
        //            evt.consume();
        //
        //            JOptionPane.showMessageDialog(rootPane, "Ingresa solo Letras", "Mensaje", WIDTH, null);
        //            //Error.setText("Ingresa Solo Numeros");
        //        } else {
        //            if (txt_Nombres.getText().length() == limite) {
        //                evt.consume();
        //            }
        //        }
    }//GEN-LAST:event_txt_subtotalKeyTyped

    private void txt_igvKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_igvKeyTyped
        //        char c = evt.getKeyChar();
        //        int limite = 20;
        //
        //        if (Character.isDigit(c)) {
        //            getToolkit().beep();
        //
        //            evt.consume();
        //
        //            JOptionPane.showMessageDialog(rootPane, "Ingresa solo Letras", "Mensaje", WIDTH, null);
        //            //Error.setText("Ingresa Solo Numeros");
        //        } else {
        //            if (txt_APaterno.getText().length() == limite) {
        //                evt.consume();
        //            }
        //        }
    }//GEN-LAST:event_txt_igvKeyTyped

    private void btn_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btn_salirActionPerformed

    private void txt_idReservaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_idReservaKeyTyped

        int limite = 6;
        if (txt_idPago.getText().length() == limite) {
            evt.consume();
        }

    }//GEN-LAST:event_txt_idReservaKeyTyped

    private void btn_consultarPagosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_consultarPagosActionPerformed

        IFrmPagosPendientes pago = new IFrmPagosPendientes();
        escritorio.add(pago);
        pago.toFront();
        pago.setVisible(true);

    }//GEN-LAST:event_btn_consultarPagosActionPerformed

    private void txt_totalKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_totalKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_totalKeyTyped

    private void btn_generarCompActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_generarCompActionPerformed

        try {
            String ruta = "src\\Reportes\\rPago.jrxml";
            JasperReport rep = JasperCompileManager.compileReport(ruta);
            
            //Se crea un objeto HashMap
            HashMap<String, Object> parametros = new HashMap<String, Object>();
            parametros.clear();
            //el nombre que se dio al parametro en JasperReport fue "p1", y se debe llamar desde Java con
            //ese mismo nombre, a su lado se pasa el valor del parametro
            parametros.put("idPago", txt_idPago.getText());

            JasperPrint reporte = JasperFillManager.fillReport(rep, parametros, cn);
            JasperViewer visor = new JasperViewer(reporte, false);
            visor.setTitle("Pago");
            visor.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPane, "Error" + e.toString());
        }

        
        
    }//GEN-LAST:event_btn_generarCompActionPerformed

    private void txt_nroComprobKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nroComprobKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_nroComprobKeyTyped

    private void chk_confirmaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chk_confirmaMouseClicked

    }//GEN-LAST:event_chk_confirmaMouseClicked

    private void btn_fechaActualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_fechaActualActionPerformed

        Calendar fecha = Calendar.getInstance();
        dc_fecha_emi.setCalendar(fecha);

    }//GEN-LAST:event_btn_fechaActualActionPerformed

    private void btn_consultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_consultarActionPerformed

        PagoDao pagoDao = new PagoDao();
        Pago pago = new Pago();
        TipoPagoDao tipoDao = new TipoPagoDao();
        TipoComprobanteDao tipoCDao = new TipoComprobanteDao();
        EstadoDao estadoDao = new EstadoDao();

        if (txt_idPago.getText().equalsIgnoreCase("")) {
            JOptionPane.showMessageDialog(rootPane, "Ingrese el número de pago a consultar", "Mensaje", WIDTH, null);
            txt_idPago.requestFocus();
        } else {
            Boolean f = pagoDao.consultar(txt_idPago.getText());
            if (f == true) {
                JOptionPane.showMessageDialog(rootPane, "Pago encontrado", "Mensaje", WIDTH, null);
                pago = pagoDao.pagoConsulta;

                txt_idPago.setText(pago.getIdPago());
                txt_idReserva.setText(pago.getIdReserva());

                //Formato en el que viene de la BD
                SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                // Valor de la BD
                String strFecha = pago.getFechaEmision();
                // java.util.date
                Date fecha = null;

                try {
                    //FECHA CON EL FORMATO UTIL
                    fecha = formato.parse(strFecha);

                } catch (ParseException ex) {
                    Logger.getLogger(IFrmReserva.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

                dc_fecha_emi.setDate(fecha);
                txt_subtotal.setText(String.valueOf(pago.getSubTotal()));
                txt_igv.setText(String.valueOf(pago.getIgv()));
                txt_total.setText(String.valueOf(pago.getTotal()));
                txt_nroPago.setText(String.valueOf(pago.getNroPago()));
                txt_nroComprob.setText(String.valueOf(pago.getNroComprobante()));

                String flag = pago.getFlag_confirma();
                if (flag.equalsIgnoreCase("1")) {
                    chk_confirma.setSelected(true);
                } else {
                    chk_confirma.setSelected(false);
                }

                cbo_estado.setSelectedItem(estadoDao.capturarDescripcion(pago.getIdEstado()));
                cbo_tipoPago.setSelectedItem(tipoDao.capturarDescripcion(pago.getIdTipoPago()));
                cbo_tipoComprobante.setSelectedItem(tipoCDao.capturarDescripcion(pago.getIdTipoComp()));
                inhabilitar();
                btn_generarComp.setEnabled(true);

            } else {
                JOptionPane.showMessageDialog(rootPane, "Pago no existe", "Mensaje", WIDTH, null);
            }

        }


    }//GEN-LAST:event_btn_consultarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancelar;
    public static javax.swing.JButton btn_consultar;
    private javax.swing.JButton btn_consultarPagos;
    private javax.swing.JButton btn_fechaActual;
    private javax.swing.JButton btn_generarComp;
    private javax.swing.JButton btn_guardar;
    private javax.swing.JButton btn_nuevo;
    private javax.swing.JButton btn_salir;
    private javax.swing.JComboBox cbo_estado;
    private javax.swing.JComboBox cbo_tipoComprobante;
    private javax.swing.JComboBox cbo_tipoPago;
    public static javax.swing.JCheckBox chk_confirma;
    public static com.toedter.calendar.JDateChooser dc_fecha_emi;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JTextField txt_idPago;
    public static javax.swing.JTextField txt_idReserva;
    public static javax.swing.JTextField txt_igv;
    private javax.swing.JTextField txt_nroComprob;
    private javax.swing.JTextField txt_nroPago;
    public static javax.swing.JTextField txt_subtotal;
    public static javax.swing.JTextField txt_total;
    // End of variables declaration//GEN-END:variables


}