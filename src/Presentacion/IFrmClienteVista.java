package Presentacion;

import Dao.ClienteDao;
import Utilidades.Conexion;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class IFrmClienteVista extends javax.swing.JInternalFrame {

    public IFrmClienteVista() {
        initComponents();
        mostrarClientes("");
    }

    Connection cn = Conexion.getConnection();
    String cod;
    String valor;
    
     void OcultarColumnas() {
        tbl_listado.getColumnModel().getColumn(2).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(2).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(2).setPreferredWidth(0);
        
        tbl_listado.getColumnModel().getColumn(3).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(3).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(3).setPreferredWidth(0);
        
        tbl_listado.getColumnModel().getColumn(4).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(4).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(4).setPreferredWidth(0);
        
        tbl_listado.getColumnModel().getColumn(9).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(9).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(9).setPreferredWidth(0);
        
        tbl_listado.getColumnModel().getColumn(10).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(10).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(10).setPreferredWidth(0);
        
        tbl_listado.getColumnModel().getColumn(11).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(11).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(11).setPreferredWidth(0);
        
        tbl_listado.getColumnModel().getColumn(13).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(13).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(13).setPreferredWidth(0);
    }
    
     private void mostrarClientes(String buscar) {
        
        try {
            DefaultTableModel modelo;
            ClienteDao cliDao = new ClienteDao();
            modelo = cliDao.mostrarVista(buscar);
            
            lbl_totalRegistros.setText("Nro. de Registros: " + Integer.toString(cliDao.totalRegistros));
            tbl_listado.setModel(modelo);
            OcultarColumnas();
            
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
        
    }
     
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_listado = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        txt_buscar = new javax.swing.JTextField();
        btn_buscar = new javax.swing.JButton();
        lbl_totalRegistros = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setTitle("Clientes Disponibles");

        jPanel2.setBackground(new java.awt.Color(235, 236, 228));
        jPanel2.setBorder(null);

        tbl_listado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_listado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_listado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_listadoMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tbl_listadoMousePressed(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_listado);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("Nombre(s) / Apellido(s)");

        txt_buscar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_buscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_buscarKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_buscarKeyTyped(evt);
            }
        });

        btn_buscar.setBackground(new java.awt.Color(51, 51, 51));
        btn_buscar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_buscar.setForeground(new java.awt.Color(255, 255, 255));
        btn_buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/buscar.png"))); // NOI18N
        btn_buscar.setText("Buscar");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });

        lbl_totalRegistros.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_totalRegistros.setText("Nro. de Registros");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Listado de Clientes");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_buscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(351, 351, 351))))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(580, 580, 580)
                        .addComponent(lbl_totalRegistros, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 712, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txt_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_buscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_totalRegistros)
                .addGap(75, 75, 75))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 743, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 727, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(10, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 339, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(12, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tbl_listadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_listadoMouseClicked

    }//GEN-LAST:event_tbl_listadoMouseClicked

    private void txt_buscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_buscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            mostrarClientes(txt_buscar.getText());
        }
    }//GEN-LAST:event_txt_buscarKeyPressed

    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed
        mostrarClientes(txt_buscar.getText());
    }//GEN-LAST:event_btn_buscarActionPerformed

    private void tbl_listadoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_listadoMousePressed

        if (evt.getClickCount() == 2) {
            int fila = tbl_listado.getSelectedRow();
            cod = tbl_listado.getValueAt(fila, 0).toString();
            valor = tbl_listado.getValueAt(fila, 2).toString() + " " + tbl_listado.getValueAt(fila, 3).toString();
            
            IFrmReserva.idCliente = cod ;
            IFrmReserva.txt_idCliente.setText(valor);
            this.dispose();
            
        }
        
    }//GEN-LAST:event_tbl_listadoMousePressed

    private void txt_buscarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_buscarKeyTyped

        
        char c = evt.getKeyChar();
        int limite = 20;
        
        if (Character.isDigit(c)) {
            getToolkit().beep();
            
            evt.consume();
            
            JOptionPane.showMessageDialog(rootPane, "Ingresa solo Letras", "Mensaje", WIDTH, null);
            //Error.setText("Ingresa Solo Numeros");
        } else {
            if (txt_buscar.getText().length() == limite) {
                evt.consume();
            }
        }
        
    }//GEN-LAST:event_txt_buscarKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_buscar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lbl_totalRegistros;
    private javax.swing.JTable tbl_listado;
    private javax.swing.JTextField txt_buscar;
    // End of variables declaration//GEN-END:variables
}
