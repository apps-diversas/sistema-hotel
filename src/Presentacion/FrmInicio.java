package Presentacion;

import Utilidades.DesktopPaneFondo;
import javax.swing.JOptionPane;

public class FrmInicio extends javax.swing.JFrame {

    public FrmInicio() {
        initComponents();
       // escritorio.setBorder(new DesktopPaneFondo());
        this.setExtendedState(FrmInicio.MAXIMIZED_BOTH);
        this.setTitle("Sistema de Reserva de Habitaciones y Gestión de Ventas - Hotel Marriot");
        mnu_productos.setEnabled(false);
    }

    //Instanciando los forms para evitar que se abran por segunda vez
    IFrmHabitacion frmHabitacion = new IFrmHabitacion();
    IFrmClientes frmClientes = new IFrmClientes();
    IFrmReserva frmReserva = new IFrmReserva();

    public void validarVentanasMantenimiento() {

        // Habitacion
        if (frmHabitacion.isShowing()) {
            this.mnu_habitaciones.setEnabled(false);
        } else {
            this.mnu_habitaciones.setEnabled(true);
        }

    }

    public void validarVentanasReserva() {

        // Clientes
        if (frmClientes.isShowing()) {
            this.mnu_clientes.setEnabled(false);
        } else {
            this.mnu_clientes.setEnabled(true);
        }
        // Reserva
        if (frmReserva.isShowing()) {
            this.mnui_reservas.setEnabled(false);
        } else {
            this.mnui_reservas.setEnabled(true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        escritorio = new javax.swing.JDesktopPane();
        jPanel1 = new javax.swing.JPanel();
        lbl_idpersona = new javax.swing.JLabel();
        lbl_nombre = new javax.swing.JLabel();
        lbl_apaterno = new javax.swing.JLabel();
        lbl_amaterno = new javax.swing.JLabel();
        lbl_acceso = new javax.swing.JLabel();
        lbl_idpersona2 = new javax.swing.JLabel();
        lbl_idpersona1 = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        mnu_sisreserva = new javax.swing.JMenu();
        mnu_cerrarSesion = new javax.swing.JMenuItem();
        mnu_salir = new javax.swing.JMenuItem();
        mnu_archivo = new javax.swing.JMenu();
        mnu_habitaciones = new javax.swing.JMenuItem();
        mnu_productos = new javax.swing.JMenuItem();
        mnu_reservas = new javax.swing.JMenu();
        mnui_reservas = new javax.swing.JMenuItem();
        mnu_clientes = new javax.swing.JMenuItem();
        mnu_pagos = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        mnu_consultas = new javax.swing.JMenu();
        mnu_rhabitaciones = new javax.swing.JMenuItem();
        mnu_rclientes = new javax.swing.JMenuItem();
        mnu_rreservas = new javax.swing.JMenuItem();
        mnu_configuraciones = new javax.swing.JMenu();
        mnu_usuarios_accesos = new javax.swing.JMenuItem();
        mnu_herramientas = new javax.swing.JMenu();
        mnu_ayuda = new javax.swing.JMenu();
        mnu_acercade = new javax.swing.JMenuItem();
        mnu_ayuda2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        escritorio.setBackground(new java.awt.Color(0, 153, 204));

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Usuario Actual", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), java.awt.Color.lightGray)); // NOI18N
        jPanel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        lbl_idpersona.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_idpersona.setForeground(new java.awt.Color(153, 153, 153));
        lbl_idpersona.setText("jLabel1");

        lbl_nombre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_nombre.setForeground(new java.awt.Color(153, 153, 153));
        lbl_nombre.setText("jLabel1");

        lbl_apaterno.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_apaterno.setForeground(new java.awt.Color(153, 153, 153));
        lbl_apaterno.setText("jLabel1");

        lbl_amaterno.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_amaterno.setForeground(new java.awt.Color(153, 153, 153));
        lbl_amaterno.setText("jLabel1");

        lbl_acceso.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_acceso.setForeground(new java.awt.Color(153, 153, 153));
        lbl_acceso.setText("jLabel1");

        lbl_idpersona2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_idpersona2.setForeground(new java.awt.Color(153, 153, 153));
        lbl_idpersona2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/marriot.jpg"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbl_acceso, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                    .addComponent(lbl_idpersona, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbl_nombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbl_apaterno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbl_amaterno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl_idpersona2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(lbl_idpersona2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(lbl_idpersona)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbl_nombre)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbl_apaterno)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbl_amaterno)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbl_acceso)))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        escritorio.add(jPanel1);
        jPanel1.setBounds(0, 510, 520, 160);
        jPanel1.getAccessibleContext().setAccessibleName("Usuario");

        lbl_idpersona1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_idpersona1.setForeground(new java.awt.Color(153, 153, 153));
        lbl_idpersona1.setText("jLabel1");
        escritorio.add(lbl_idpersona1);
        lbl_idpersona1.setBounds(190, 540, 110, 110);

        mnu_sisreserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/home2.png"))); // NOI18N
        mnu_sisreserva.setMnemonic('f');
        mnu_sisreserva.setText("Sisreserva");
        mnu_sisreserva.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        mnu_cerrarSesion.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_MASK));
        mnu_cerrarSesion.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mnu_cerrarSesion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/cerrarSesion.png"))); // NOI18N
        mnu_cerrarSesion.setText("Cerrar Sesión");
        mnu_cerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnu_cerrarSesionActionPerformed(evt);
            }
        });
        mnu_sisreserva.add(mnu_cerrarSesion);

        mnu_salir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.ALT_MASK));
        mnu_salir.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mnu_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/salir.png"))); // NOI18N
        mnu_salir.setText("Salir");
        mnu_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnu_salirActionPerformed(evt);
            }
        });
        mnu_sisreserva.add(mnu_salir);

        menuBar.add(mnu_sisreserva);

        mnu_archivo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/archivo.png"))); // NOI18N
        mnu_archivo.setMnemonic('e');
        mnu_archivo.setText("Mantenimiento");
        mnu_archivo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mnu_archivo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mnu_archivoMouseClicked(evt);
            }
        });

        mnu_habitaciones.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.CTRL_MASK));
        mnu_habitaciones.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mnu_habitaciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/habitaciones.png"))); // NOI18N
        mnu_habitaciones.setMnemonic('t');
        mnu_habitaciones.setText("Habitaciones");
        mnu_habitaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnu_habitacionesActionPerformed(evt);
            }
        });
        mnu_archivo.add(mnu_habitaciones);

        mnu_productos.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        mnu_productos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mnu_productos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/productos.png"))); // NOI18N
        mnu_productos.setMnemonic('y');
        mnu_productos.setText("Productos");
        mnu_productos.setEnabled(false);
        mnu_productos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnu_productosActionPerformed(evt);
            }
        });
        mnu_archivo.add(mnu_productos);

        menuBar.add(mnu_archivo);

        mnu_reservas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/reserva.png"))); // NOI18N
        mnu_reservas.setMnemonic('h');
        mnu_reservas.setText("Reservas");
        mnu_reservas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mnu_reservas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mnu_reservasMouseClicked(evt);
            }
        });

        mnui_reservas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
        mnui_reservas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mnui_reservas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/reservas_consumos.png"))); // NOI18N
        mnui_reservas.setMnemonic('c');
        mnui_reservas.setText("Reservas");
        mnui_reservas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnui_reservasActionPerformed(evt);
            }
        });
        mnu_reservas.add(mnui_reservas);

        mnu_clientes.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        mnu_clientes.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mnu_clientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/clientes.png"))); // NOI18N
        mnu_clientes.setMnemonic('a');
        mnu_clientes.setText("Clientes");
        mnu_clientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnu_clientesActionPerformed(evt);
            }
        });
        mnu_reservas.add(mnu_clientes);

        mnu_pagos.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        mnu_pagos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mnu_pagos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/pagos.png"))); // NOI18N
        mnu_pagos.setText("Pagos");
        mnu_pagos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnu_pagosActionPerformed(evt);
            }
        });
        mnu_reservas.add(mnu_pagos);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/consumo.png"))); // NOI18N
        jMenuItem1.setText("Consumos");
        jMenuItem1.setToolTipText("");
        jMenuItem1.setEnabled(false);
        mnu_reservas.add(jMenuItem1);

        menuBar.add(mnu_reservas);

        mnu_consultas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/consultas.png"))); // NOI18N
        mnu_consultas.setText("Toma de Decisiones");
        mnu_consultas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        mnu_rhabitaciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/RHabitaciones.png"))); // NOI18N
        mnu_rhabitaciones.setText("Habitaciones");
        mnu_rhabitaciones.setEnabled(false);
        mnu_rhabitaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnu_rhabitacionesActionPerformed(evt);
            }
        });
        mnu_consultas.add(mnu_rhabitaciones);

        mnu_rclientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/RClientes.png"))); // NOI18N
        mnu_rclientes.setText("Clientes");
        mnu_rclientes.setEnabled(false);
        mnu_rclientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnu_rclientesActionPerformed(evt);
            }
        });
        mnu_consultas.add(mnu_rclientes);

        mnu_rreservas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/RReservas.png"))); // NOI18N
        mnu_rreservas.setText("Reservas");
        mnu_rreservas.setEnabled(false);
        mnu_rreservas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnu_rreservasActionPerformed(evt);
            }
        });
        mnu_consultas.add(mnu_rreservas);

        menuBar.add(mnu_consultas);

        mnu_configuraciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/configuraciones.png"))); // NOI18N
        mnu_configuraciones.setText("Configuraciones");
        mnu_configuraciones.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        mnu_usuarios_accesos.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.CTRL_MASK));
        mnu_usuarios_accesos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mnu_usuarios_accesos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/usuarios_accesos.png"))); // NOI18N
        mnu_usuarios_accesos.setText("Usuarios y Accesos");
        mnu_usuarios_accesos.setEnabled(false);
        mnu_usuarios_accesos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnu_usuarios_accesosActionPerformed(evt);
            }
        });
        mnu_configuraciones.add(mnu_usuarios_accesos);

        menuBar.add(mnu_configuraciones);

        mnu_herramientas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/herramientas.png"))); // NOI18N
        mnu_herramientas.setText("Herramientas");
        mnu_herramientas.setEnabled(false);
        mnu_herramientas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        menuBar.add(mnu_herramientas);

        mnu_ayuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ayuda1.png"))); // NOI18N
        mnu_ayuda.setText("Ayuda");
        mnu_ayuda.setEnabled(false);
        mnu_ayuda.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        mnu_acercade.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        mnu_acercade.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mnu_acercade.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/acercade.png"))); // NOI18N
        mnu_acercade.setText("Acerca de Marriot");
        mnu_acercade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnu_acercadeActionPerformed(evt);
            }
        });
        mnu_ayuda.add(mnu_acercade);
        mnu_acercade.getAccessibleContext().setAccessibleDescription("");

        mnu_ayuda2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.CTRL_MASK));
        mnu_ayuda2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mnu_ayuda2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ayuda2.png"))); // NOI18N
        mnu_ayuda2.setText("Ayuda");
        mnu_ayuda2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnu_ayuda2ActionPerformed(evt);
            }
        });
        mnu_ayuda.add(mnu_ayuda2);

        menuBar.add(mnu_ayuda);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(escritorio, javax.swing.GroupLayout.DEFAULT_SIZE, 1030, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(escritorio, javax.swing.GroupLayout.PREFERRED_SIZE, 674, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mnu_usuarios_accesosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnu_usuarios_accesosActionPerformed

//        FrmTrabajador form = new FrmTrabajador();
//        escritorio.add(form);
//        form.toFront();
//        form.setVisible(true);

    }//GEN-LAST:event_mnu_usuarios_accesosActionPerformed

    private void mnu_ayuda2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnu_ayuda2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnu_ayuda2ActionPerformed

    private void mnu_habitacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnu_habitacionesActionPerformed

        escritorio.add(frmHabitacion);
        frmHabitacion.toFront();
        frmHabitacion.setVisible(true);

    }//GEN-LAST:event_mnu_habitacionesActionPerformed

    private void mnu_productosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnu_productosActionPerformed

//        FrmProducto form = new FrmProducto();
//        escritorio.add(form);
//        form.toFront();
//        form.setVisible(true);
//        
    }//GEN-LAST:event_mnu_productosActionPerformed

    private void mnu_clientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnu_clientesActionPerformed

//        IFrmClientes frmClientes = new IFrmClientes();
//        escritorio.add(frmClientes);
//        frmClientes.toFront();
//        frmClientes.setVisible(true);
        escritorio.add(frmClientes);
        frmClientes.toFront();
        frmClientes.setVisible(true);

    }//GEN-LAST:event_mnu_clientesActionPerformed

    private void mnui_reservasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnui_reservasActionPerformed

        escritorio.add(frmReserva);
        frmReserva.toFront();
        frmReserva.setVisible(true);

//        FrmReserva resv = new FrmReserva();
//        escritorio.add(resv);
//        resv.toFront();
//        resv.setVisible(true);
//        
//        FrmReserva.txt_idtrabajador.setText(lbl_idpersona.getText());
//        FrmReserva.txt_trabajador.setText(lbl_nombre.getText()+" "+lbl_apaterno.getText());
//        FrmReserva.idusuario = Integer.parseInt(lbl_idpersona.getText());

    }//GEN-LAST:event_mnui_reservasActionPerformed

    private void mnu_cerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnu_cerrarSesionActionPerformed

        int rpta = JOptionPane.showConfirmDialog(null, "¿Cerrar la sesión actual?", "Cerrar Sesión", JOptionPane.YES_NO_OPTION);

        if (rpta == JOptionPane.YES_OPTION) {
            this.dispose();
            FrmUsuarioLogin form = new FrmUsuarioLogin();
            form.toFront();
            form.setVisible(true);
        }
    }//GEN-LAST:event_mnu_cerrarSesionActionPerformed

    private void mnu_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnu_salirActionPerformed
        int rpta = JOptionPane.showConfirmDialog(null, "¿Seguro que desea salir?", "Salir", JOptionPane.YES_NO_OPTION);

        if (rpta == JOptionPane.YES_OPTION) {
            this.dispose();
        }
    }//GEN-LAST:event_mnu_salirActionPerformed

    private void mnu_archivoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnu_archivoMouseClicked
        validarVentanasMantenimiento();
    }//GEN-LAST:event_mnu_archivoMouseClicked

    private void mnu_acercadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnu_acercadeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnu_acercadeActionPerformed

    private void mnu_reservasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnu_reservasMouseClicked
        validarVentanasReserva();
    }//GEN-LAST:event_mnu_reservasMouseClicked

    private void mnu_rhabitacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnu_rhabitacionesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnu_rhabitacionesActionPerformed

    private void mnu_rclientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnu_rclientesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnu_rclientesActionPerformed

    private void mnu_rreservasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnu_rreservasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnu_rreservasActionPerformed

    private void mnu_pagosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnu_pagosActionPerformed

        IFrmPago pago = new IFrmPago();
        escritorio.add(pago);
        pago.toFront();
        pago.setVisible(true);
        
    }//GEN-LAST:event_mnu_pagosActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmInicio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JDesktopPane escritorio;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    public static javax.swing.JLabel lbl_acceso;
    public static javax.swing.JLabel lbl_amaterno;
    public static javax.swing.JLabel lbl_apaterno;
    public static javax.swing.JLabel lbl_idpersona;
    public static javax.swing.JLabel lbl_idpersona1;
    public static javax.swing.JLabel lbl_idpersona2;
    public static javax.swing.JLabel lbl_nombre;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem mnu_acercade;
    public static javax.swing.JMenu mnu_archivo;
    private javax.swing.JMenu mnu_ayuda;
    private javax.swing.JMenuItem mnu_ayuda2;
    private javax.swing.JMenuItem mnu_cerrarSesion;
    private javax.swing.JMenuItem mnu_clientes;
    public static javax.swing.JMenu mnu_configuraciones;
    private javax.swing.JMenu mnu_consultas;
    private javax.swing.JMenuItem mnu_habitaciones;
    private javax.swing.JMenu mnu_herramientas;
    private javax.swing.JMenuItem mnu_pagos;
    private javax.swing.JMenuItem mnu_productos;
    private javax.swing.JMenuItem mnu_rclientes;
    private javax.swing.JMenu mnu_reservas;
    private javax.swing.JMenuItem mnu_rhabitaciones;
    private javax.swing.JMenuItem mnu_rreservas;
    private javax.swing.JMenuItem mnu_salir;
    private javax.swing.JMenu mnu_sisreserva;
    private javax.swing.JMenuItem mnu_usuarios_accesos;
    private javax.swing.JMenuItem mnui_reservas;
    // End of variables declaration//GEN-END:variables

}
