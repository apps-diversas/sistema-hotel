package Presentacion;

import Dao.EstadoDao;
import Dao.ReservaDao;
import Datos.Reserva;
import static Presentacion.FrmInicio.escritorio;
import Utilidades.Conexion;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

public class IFrmReserva extends javax.swing.JInternalFrame {

    public IFrmReserva() {
        initComponents();
        inhabilitar();
    }

    public static String idReserva = "";
    public static String idCliente = "";
    public static String idEmpleado = "";
    String accion = "guardar";
    Connection cn = Conexion.getConnection();

    public void inhabilitar() {
        txt_totalPagar.setEnabled(false);
        txt_confirma.setEnabled(false);
        dc_fecha_registro.setEnabled(false);
        btn_fechaActual.setEnabled(false);
        btn_buscarCliente.setEnabled(false);
        chk_confirma.setEnabled(false);
        cbo_horas.setEnabled(false);
        cbo_minutos.setEnabled(false);
        cbo_segundos.setEnabled(false);

        txt_totalPagar.setText("0.00");
        txt_confirma.setText("0.00");

        btn_consultarReserva.setEnabled(true);
        btn_nuevo.setEnabled(true);
        btn_cancelar.setEnabled(false);
        btn_eliminar.setEnabled(false);
        btn_guardar.setEnabled(false);
        btn_imprimir.setEnabled(false);
        //btn_pagoConfirma.setEnabled(false);
        btn_detalleReserva.setEnabled(false);
        btn_consumo.setEnabled(false);
        btn_Pago.setEnabled(false);
        btn_finalizar.setEnabled(false);

        txt_idReserva.setText("");
        txt_idCliente.setText("");
        cargarEstadoInicio();
    }

    public void habilitar_nuevo() {
        txt_totalPagar.setEnabled(false);
        txt_confirma.setEnabled(false);
        dc_fecha_registro.setEnabled(true);
        btn_fechaActual.setEnabled(true);
        btn_buscarCliente.setEnabled(true);
        btn_consultarReserva.setEnabled(false);
        cbo_horas.setEnabled(true);
        cbo_minutos.setEnabled(true);
        cbo_segundos.setEnabled(true);
        txt_totalPagar.setText("0.00");

        btn_nuevo.setEnabled(false);
        btn_guardar.setEnabled(true);
        btn_cancelar.setEnabled(true);
        btn_eliminar.setEnabled(false);

        btn_imprimir.setEnabled(false);
        //btn_pagoConfirma.setEnabled(false);
        btn_detalleReserva.setEnabled(false);
        btn_consumo.setEnabled(false);
        btn_Pago.setEnabled(false);
        btn_finalizar.setEnabled(false);

    }

    public void habilitar_consulta() {
        txt_totalPagar.setEnabled(false);
        txt_confirma.setEnabled(false);
        dc_fecha_registro.setEnabled(true);
        btn_fechaActual.setEnabled(true);
        btn_buscarCliente.setEnabled(true);
        btn_consultarReserva.setEnabled(false);
        cbo_horas.setEnabled(true);
        cbo_minutos.setEnabled(true);
        cbo_segundos.setEnabled(true);

        txt_totalPagar.setText("0.00");

        btn_nuevo.setEnabled(false);
        btn_guardar.setEnabled(true);
        btn_cancelar.setEnabled(true);
        btn_eliminar.setEnabled(true);

        btn_imprimir.setEnabled(false);
        //btn_pagoConfirma.setEnabled(false);
        btn_detalleReserva.setEnabled(false);
        btn_consumo.setEnabled(false);
        btn_Pago.setEnabled(false);
        btn_finalizar.setEnabled(false);

    }

    public void cargarEstadoInicio() {
        ReservaDao resDao = new ReservaDao();
        cbo_estado.setModel(resDao.mostrarEstadoInicio());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txt_idReserva = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txt_idCliente = new javax.swing.JTextField();
        btn_nuevo = new javax.swing.JButton();
        btn_guardar = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txt_totalPagar = new javax.swing.JTextField();
        dc_fecha_registro = new com.toedter.calendar.JDateChooser();
        jLabel15 = new javax.swing.JLabel();
        btn_consultarReserva = new javax.swing.JButton();
        btn_eliminar = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        chk_confirma = new javax.swing.JCheckBox();
        txt_confirma = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        btn_salir = new javax.swing.JButton();
        btn_fechaActual = new javax.swing.JButton();
        btn_buscarCliente = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        cbo_horas = new javax.swing.JComboBox();
        cbo_minutos = new javax.swing.JComboBox();
        cbo_segundos = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        cbo_estado = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        btn_detalleReserva = new javax.swing.JButton();
        btn_consumo = new javax.swing.JButton();
        btn_Pago = new javax.swing.JButton();
        btn_imprimir = new javax.swing.JButton();
        btn_finalizar = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(235, 236, 228));

        txt_idReserva.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_idReserva.setMaximumSize(new java.awt.Dimension(6, 6));
        txt_idReserva.setMinimumSize(new java.awt.Dimension(6, 6));
        txt_idReserva.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_idReservaKeyTyped(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Cliente");

        txt_idCliente.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_idCliente.setEnabled(false);
        txt_idCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_idClienteKeyTyped(evt);
            }
        });

        btn_nuevo.setBackground(new java.awt.Color(0, 0, 0));
        btn_nuevo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_nuevo.setForeground(new java.awt.Color(255, 255, 255));
        btn_nuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/plus.png"))); // NOI18N
        btn_nuevo.setText("Nuevo");
        btn_nuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_nuevoActionPerformed(evt);
            }
        });

        btn_guardar.setBackground(new java.awt.Color(0, 0, 0));
        btn_guardar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_guardar.setForeground(new java.awt.Color(255, 255, 255));
        btn_guardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ok1.png"))); // NOI18N
        btn_guardar.setText("Guardar");
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });

        btn_cancelar.setBackground(new java.awt.Color(0, 0, 0));
        btn_cancelar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_cancelar.setForeground(new java.awt.Color(255, 255, 255));
        btn_cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/cancelar.png"))); // NOI18N
        btn_cancelar.setText("Cancelar");
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Código");

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("Total a Pagar");

        txt_totalPagar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_totalPagar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_totalPagarKeyTyped(evt);
            }
        });

        dc_fecha_registro.setDateFormatString("dd/MM/yyyy");

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel15.setText("Fecha");

        btn_consultarReserva.setBackground(new java.awt.Color(255, 64, 64));
        btn_consultarReserva.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_consultarReserva.setForeground(new java.awt.Color(255, 255, 255));
        btn_consultarReserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/buscar.png"))); // NOI18N
        btn_consultarReserva.setText("Consultar");
        btn_consultarReserva.setToolTipText("");
        btn_consultarReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_consultarReservaActionPerformed(evt);
            }
        });

        btn_eliminar.setBackground(new java.awt.Color(0, 0, 0));
        btn_eliminar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_eliminar.setForeground(new java.awt.Color(255, 255, 255));
        btn_eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/eliminar.png"))); // NOI18N
        btn_eliminar.setText("Anular");
        btn_eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_eliminarActionPerformed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel16.setText("Estado");

        jPanel2.setBackground(new java.awt.Color(95, 158, 160));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.gray, java.awt.Color.gray));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));

        chk_confirma.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                chk_confirmaMouseClicked(evt);
            }
        });

        txt_confirma.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_confirma.setMaximumSize(new java.awt.Dimension(6, 6));
        txt_confirma.setMinimumSize(new java.awt.Dimension(6, 6));
        txt_confirma.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_confirmaKeyTyped(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Confirmación - 30% del Total a pagar");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(chk_confirma)
                        .addGap(18, 18, 18)
                        .addComponent(txt_confirma, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(21, Short.MAX_VALUE)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_confirma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chk_confirma))
                .addContainerGap())
        );

        btn_salir.setBackground(new java.awt.Color(0, 0, 0));
        btn_salir.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_salir.setForeground(new java.awt.Color(255, 255, 255));
        btn_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/home.png"))); // NOI18N
        btn_salir.setText("Salir");
        btn_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salirActionPerformed(evt);
            }
        });

        btn_fechaActual.setBackground(new java.awt.Color(255, 64, 64));
        btn_fechaActual.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_fechaActual.setForeground(new java.awt.Color(255, 255, 255));
        btn_fechaActual.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/horaActual.png"))); // NOI18N
        btn_fechaActual.setToolTipText("");
        btn_fechaActual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_fechaActualActionPerformed(evt);
            }
        });

        btn_buscarCliente.setBackground(new java.awt.Color(255, 64, 64));
        btn_buscarCliente.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_buscarCliente.setForeground(new java.awt.Color(255, 255, 255));
        btn_buscarCliente.setText("...");
        btn_buscarCliente.setToolTipText("");
        btn_buscarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarClienteActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel13.setText("Hora");

        cbo_horas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24" }));

        cbo_minutos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", " " }));

        cbo_segundos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", " " }));

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel14.setText("(hh:mm:ss)");

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel17.setText("(dd/MM/yyyy)");

        cbo_estado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cbo_estado.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel2)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel14))
                            .addComponent(jLabel16)
                            .addComponent(jLabel12)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel17)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btn_eliminar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_cancelar))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(dc_fecha_registro, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(txt_idCliente)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(btn_buscarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(2, 2, 2)))
                                        .addComponent(btn_fechaActual, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(46, 46, 46))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(txt_idReserva, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btn_consultarReserva)
                                        .addGap(12, 12, 12))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_totalPagar, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(cbo_horas, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cbo_minutos, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cbo_segundos, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(cbo_estado, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_nuevo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_guardar)))
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_salir, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel10)
                                    .addComponent(txt_idReserva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn_consultarReserva))
                                .addGap(7, 7, 7)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txt_idCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2)
                                    .addComponent(btn_buscarCliente))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(11, 11, 11)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(dc_fecha_registro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btn_fechaActual)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel15)
                                            .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_totalPagar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(cbo_estado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(btn_cancelar)
                                    .addComponent(btn_eliminar)
                                    .addComponent(btn_guardar)
                                    .addComponent(btn_nuevo)
                                    .addComponent(btn_salir)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(122, 122, 122)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbo_segundos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbo_minutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbo_horas, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(79, 79, 79));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Reserva");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(235, 236, 228));
        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(193, 205, 205)));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Registro de reservas");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel5)
        );

        jPanel4.setBackground(new java.awt.Color(79, 79, 79));

        btn_detalleReserva.setBackground(new java.awt.Color(0, 0, 0));
        btn_detalleReserva.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_detalleReserva.setForeground(new java.awt.Color(255, 255, 255));
        btn_detalleReserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/detalleReserva.png"))); // NOI18N
        btn_detalleReserva.setText("  Detalle de Reserva");
        btn_detalleReserva.setToolTipText("");
        btn_detalleReserva.setBorder(null);
        btn_detalleReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_detalleReservaActionPerformed(evt);
            }
        });

        btn_consumo.setBackground(new java.awt.Color(0, 0, 0));
        btn_consumo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_consumo.setForeground(new java.awt.Color(255, 255, 255));
        btn_consumo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/detalleConsumo.png"))); // NOI18N
        btn_consumo.setText("Consumo de Productos");
        btn_consumo.setToolTipText("");

        btn_Pago.setBackground(new java.awt.Color(0, 0, 0));
        btn_Pago.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_Pago.setForeground(new java.awt.Color(255, 255, 255));
        btn_Pago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/pagoReserva.png"))); // NOI18N
        btn_Pago.setText("     Realizar Pago");
        btn_Pago.setToolTipText("");
        btn_Pago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_PagoActionPerformed(evt);
            }
        });

        btn_imprimir.setBackground(new java.awt.Color(0, 0, 0));
        btn_imprimir.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_imprimir.setForeground(new java.awt.Color(255, 255, 255));
        btn_imprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/detalleImprimir.png"))); // NOI18N
        btn_imprimir.setText("  Imprimir");
        btn_imprimir.setToolTipText("");
        btn_imprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_imprimirActionPerformed(evt);
            }
        });

        btn_finalizar.setBackground(new java.awt.Color(255, 64, 64));
        btn_finalizar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_finalizar.setForeground(new java.awt.Color(255, 255, 255));
        btn_finalizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/reservaFinalizar.png"))); // NOI18N
        btn_finalizar.setText("Finalizar Alquiler");
        btn_finalizar.setToolTipText("");
        btn_finalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_finalizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_Pago, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_detalleReserva, javax.swing.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_consumo, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_imprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addComponent(btn_finalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_detalleReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_consumo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_Pago, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_imprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btn_finalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 706, Short.MAX_VALUE))
                .addContainerGap(11, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btn_salirActionPerformed

    private void txt_confirmaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_confirmaKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_confirmaKeyTyped

    private void btn_eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_eliminarActionPerformed

        Reserva res = new Reserva();
        ReservaDao resDao = new ReservaDao();

        res.setIdReserva(txt_idReserva.getText());

        int rpta = JOptionPane.showConfirmDialog(null, "¿Seguro que desea anular la reserva?", "Eliminar", JOptionPane.YES_NO_OPTION);
        if (rpta == JOptionPane.YES_OPTION) {
            boolean f = resDao.eliminar(res);
            if (f == true) {
                boolean f2 = resDao.eliminarDetalle(res);
                if (f2 == true) {
                    JOptionPane.showMessageDialog(rootPane, "Reserva anulada con éxito", "Registrar", WIDTH, null);
                    inhabilitar();
                }
            } else {
                JOptionPane.showMessageDialog(rootPane, "No se pudo anular la reserva... Verifique", "Registrar", WIDTH, null);
            }
        }


    }//GEN-LAST:event_btn_eliminarActionPerformed

    private void txt_totalPagarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_totalPagarKeyTyped

    }//GEN-LAST:event_txt_totalPagarKeyTyped

    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        inhabilitar();
        accion = "guardar";
        btn_guardar.setText("Guardar");
    }//GEN-LAST:event_btn_cancelarActionPerformed

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed

        //validarCasillas();
        ReservaDao resDao = new ReservaDao();

        String flagConfirma = "0";
        Reserva res = new Reserva();

        Calendar cal;
        int d, m, a, h, min;
        cal = dc_fecha_registro.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        h = cal.get(Calendar.HOUR_OF_DAY);
        min = cal.get(Calendar.MINUTE);

        String fecha = cal.get(cal.DATE) + "/" + (cal.get(cal.MONTH) + 1) + "/" + cal.get(cal.YEAR);
        String hora = cbo_horas.getSelectedItem().toString() + ":" + cbo_minutos.getSelectedItem().toString();
        String fechaFinal = fecha + " " + hora;

        if (a < 115 || a > 116 || m > 11 || m < 0 || d > 31 || d < 01) {
            JOptionPane.showMessageDialog(rootPane, "Debe ingresar una fecha válida", "Aviso", WIDTH, null);
            return;
        } else {

            if (chk_confirma.isSelected()) {
                flagConfirma = "1";
                res.setTotalConfirma(Double.parseDouble(txt_confirma.getText()));
            } else {
                flagConfirma = "0";
                res.setTotalConfirma(0.00);
            }

            res.setIdReserva(txt_idReserva.getText());
            res.setIdCliente(idCliente);
            res.setFechaRegistro(fechaFinal);
            res.setTotal(Double.parseDouble(txt_totalPagar.getText()));
            res.setFlagConfirma(flagConfirma);
            res.setIdEmpleado(idEmpleado);

            EstadoDao e = new EstadoDao();
            String idEstado = e.capturarIdEstado(cbo_estado.getSelectedItem().toString());
            res.setIdEstado(idEstado);
//        res.setIdEstado(estado.getIdEstado());

            if ("guardar".equals(accion)) {

                int rpta = JOptionPane.showConfirmDialog(null, "¿Seguro que desea registrar la reserva?", "Registrar", JOptionPane.YES_NO_OPTION);
                if (rpta == JOptionPane.YES_OPTION) {
                    boolean f = resDao.insertar(res);
                    if (f == true) {
                        JOptionPane.showMessageDialog(rootPane, "Reserva registrada con éxito", "Registrar", WIDTH, null);
                        inhabilitar();
                    } else {
                        JOptionPane.showMessageDialog(rootPane, "No se pudo registrar la reserva... Verifique", "Registrar", WIDTH, null);
                    }
                }
            }

            if ("editar".equals(accion)) {

                int rpta = JOptionPane.showConfirmDialog(null, "¿Seguro que desea modificar la reserva?", "Modificar", JOptionPane.YES_NO_OPTION);
                if (rpta == JOptionPane.YES_OPTION) {

                    //res.setIdCliente(idCliente);
                    if (idCliente == "") {
                        res.setIdCliente(resDao.capturaCodigoCliente(res.getIdReserva()));
                    } else {
                        res.setIdCliente(idCliente);
                    }
                    boolean f = resDao.editar(res);

                    if (f == true) {
                        JOptionPane.showMessageDialog(rootPane, "Reserva modificada con éxito", "Modificar", WIDTH, null);
                        inhabilitar();
                        btn_guardar.setText("Guardar");
                        btn_eliminar.setEnabled(false);
                    } else {
                        JOptionPane.showMessageDialog(rootPane, "No se pudo modificar la reserva... Verifique", "Registrar", WIDTH, null);
                    }
                }
            }
        }


    }//GEN-LAST:event_btn_guardarActionPerformed

    private void btn_nuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_nuevoActionPerformed
        habilitar_nuevo();
        cargarEstadoInicio();
    }//GEN-LAST:event_btn_nuevoActionPerformed

    private void txt_idClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_idClienteKeyTyped
        //
        //        char c = evt.getKeyChar();
        //        int limite = 8;
        //
        //        if (Character.isLetter(c)) {
        //            getToolkit().beep();
        //
        //            evt.consume();
        //
        //            JOptionPane.showMessageDialog(rootPane, "Ingresa solo Numeros", "Mensaje", WIDTH, null);
        //            //Error.setText("Ingresa Solo Numeros");
        //        } else {
        //            if (txt_Dni.getText().length() == limite) {
        //                evt.consume();
        //            }
        //        }
    }//GEN-LAST:event_txt_idClienteKeyTyped

    private void txt_idReservaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_idReservaKeyTyped
        int limite = 6;
        if (txt_idReserva.getText().length() == limite) {
            evt.consume();
        }
    }//GEN-LAST:event_txt_idReservaKeyTyped

    private void btn_buscarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarClienteActionPerformed

        IFrmClienteVista vCli = new IFrmClienteVista();
        escritorio.add(vCli);
        vCli.toFront();
        vCli.setVisible(true);

    }//GEN-LAST:event_btn_buscarClienteActionPerformed

    private void btn_fechaActualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_fechaActualActionPerformed

        Calendar fecha = Calendar.getInstance();
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);

        cbo_horas.setSelectedItem(String.valueOf(hora));
        cbo_minutos.setSelectedItem(String.valueOf(minuto));
        cbo_segundos.setSelectedItem(String.valueOf(segundo));

        dc_fecha_registro.setCalendar(fecha);


    }//GEN-LAST:event_btn_fechaActualActionPerformed

    private void btn_consultarReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_consultarReservaActionPerformed
        Reserva res;
        ReservaDao resDao = new ReservaDao();
        boolean existe = resDao.consultar(txt_idReserva.getText());

        if (existe == true) {
            habilitar_consulta();
            btn_consultarReserva.setEnabled(true);
            btn_detalleReserva.setEnabled(true);
            accion = "editar";
            btn_guardar.setText("Editar");

            JOptionPane.showMessageDialog(rootPane, "Reserva encontrada", "Aviso", WIDTH, null);

            //chk_confirma.setEnabled(true);
            //btn_pagoConfirma.setEnabled(true);
            res = resDao.resConsulta;
            txt_idReserva.setText(res.getIdReserva());
            txt_idCliente.setText(resDao.clienteDes);

            //Formato en el que viene de la BD
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // Valor de la BD
            String strFecha = res.getFechaRegistro();
            // java.util.date
            Date fecha = null;
            //Recuperamos y cortamos la hora
            String h = strFecha.substring(11, 13);
            String m = strFecha.substring(14, 16);
            String s = strFecha.substring(17, 19);

            try {
                //FECHA CON EL FORMATO UTIL
                fecha = formato.parse(strFecha);
            } catch (ParseException ex) {
                Logger.getLogger(IFrmReserva.class.getName()).log(Level.SEVERE, null, ex);
            }

            dc_fecha_registro.setDate(fecha);
            cbo_horas.setSelectedItem(h);
            cbo_minutos.setSelectedItem(m);
            cbo_segundos.setSelectedItem(s);

            chk_confirma.setEnabled(true);
            if (res.getTotal() > 0.0) {
                btn_Pago.setEnabled(true);
                btn_imprimir.setEnabled(true);
            }

            if (res.getTotal() > 0.0 && res.getFlagConfirma() == "1") {
                //btn_Pago.setEnabled(true);
                chk_confirma.setSelected(true);
                DecimalFormat df = new DecimalFormat("#.##");
                //df.format(0.912385);
                txt_confirma.setText(String.valueOf(df.format(res.getTotal() * 0.3)));
            } else {
                //btn_Pago.setEnabled(false);
                //chk_confirma.setEnabled(false);
                chk_confirma.setSelected(false);
                txt_confirma.setEnabled(false);
                txt_confirma.setText("0.00");
            }
            txt_totalPagar.setText(String.valueOf(res.getTotal()));
            cbo_estado.setSelectedItem(resDao.consultaDesEstado);

            String flag = res.getFlagConfirma().trim();
            if (flag.equalsIgnoreCase("0")) {
            }
            if (flag.equalsIgnoreCase("1")) {
                chk_confirma.setSelected(true);
                txt_confirma.setText(String.valueOf(res.getTotalConfirma()));
                //btn_pagoConfirma.setEnabled(true);
            }

            if (resDao.consultaDesEstado.equals("ANULADA")) {
                btn_eliminar.setEnabled(false);
            } else {
                btn_eliminar.setEnabled(true);
            }
            
            int resultado = resDao.PagosPendientes(txt_idReserva.getText());
            if (resultado>0) {
                btn_finalizar.setEnabled(true);
            } else {
                btn_finalizar.setEnabled(false);
            }
            if (resDao.consultaDesEstado.equals("FINALIZADA")) {
                btn_finalizar.setEnabled(false);
            } else {
                btn_finalizar.setEnabled(true);
            }

        } else {
            JOptionPane.showMessageDialog(rootPane, "La reserva no existe...Verifique", "Aviso", WIDTH, null);
            txt_idReserva.requestFocus();
        }

    }//GEN-LAST:event_btn_consultarReservaActionPerformed

    private void chk_confirmaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chk_confirmaMouseClicked

    }//GEN-LAST:event_chk_confirmaMouseClicked

    private void btn_detalleReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_detalleReservaActionPerformed

        idReserva = txt_idReserva.getText();
        IFrmDetalleReserva detRes = new IFrmDetalleReserva();
        escritorio.add(detRes);
        detRes.toFront();
        detRes.setVisible(true);

    }//GEN-LAST:event_btn_detalleReservaActionPerformed

    private void btn_PagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_PagoActionPerformed

        IFrmPago pago = new IFrmPago();
        escritorio.add(pago);
        pago.toFront();
        pago.setVisible(true);
        pago.txt_idReserva.setText(txt_idReserva.getText());
        pago.btn_consultar.setEnabled(false);

    }//GEN-LAST:event_btn_PagoActionPerformed

    private void btn_imprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_imprimirActionPerformed

        try {
            String ruta = "src\\Reportes\\rReserva.jrxml";
            JasperReport rep = JasperCompileManager.compileReport(ruta);
            
            //Se crea un objeto HashMap
            HashMap<String, Object> parametros = new HashMap<String, Object>();
            parametros.clear();
            //el nombre que se dio al parametro en JasperReport fue "p1", y se debe llamar desde Java con
            //ese mismo nombre, a su lado se pasa el valor del parametro
            parametros.put("idReserva", txt_idReserva.getText());

            JasperPrint reporte = JasperFillManager.fillReport(rep, parametros, cn);
            JasperViewer visor = new JasperViewer(reporte, false);
            visor.setTitle("Reserva");
            visor.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPane, "Error" + e.toString());
        }

    }//GEN-LAST:event_btn_imprimirActionPerformed

    private void btn_finalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_finalizarActionPerformed

        Reserva res = new Reserva();
        ReservaDao resDao = new ReservaDao();
        
        String idReserva = txt_idReserva.getText();
        res.setIdReserva(txt_idReserva.getText());
        
        int rpta = JOptionPane.showConfirmDialog(null, "¿Seguro que desea finalizar el alquiler?", "Finalizar", JOptionPane.YES_NO_OPTION);
                if (rpta == JOptionPane.YES_OPTION) {
                    boolean f = resDao.finalizarReserva(idReserva);
                    if (f == true) {
                        JOptionPane.showMessageDialog(rootPane, "Alquiler finalizado con éxito", "Finalizar", WIDTH, null);
                        resDao.eliminarDetalle(res);
                        inhabilitar();
                    } else {
                        JOptionPane.showMessageDialog(rootPane, "No se pudo finalizar el alquiler... Verifique", "Finalizar", WIDTH, null);
                    }
                }
        
        
    }//GEN-LAST:event_btn_finalizarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Pago;
    private javax.swing.JButton btn_buscarCliente;
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_consultarReserva;
    private javax.swing.JButton btn_consumo;
    private javax.swing.JButton btn_detalleReserva;
    public static javax.swing.JButton btn_eliminar;
    private javax.swing.JButton btn_fechaActual;
    private javax.swing.JButton btn_finalizar;
    private javax.swing.JButton btn_guardar;
    private javax.swing.JButton btn_imprimir;
    private javax.swing.JButton btn_nuevo;
    private javax.swing.JButton btn_salir;
    private javax.swing.JComboBox cbo_estado;
    private javax.swing.JComboBox cbo_horas;
    private javax.swing.JComboBox cbo_minutos;
    private javax.swing.JComboBox cbo_segundos;
    private javax.swing.JCheckBox chk_confirma;
    private com.toedter.calendar.JDateChooser dc_fecha_registro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    public static javax.swing.JTextField txt_confirma;
    public static javax.swing.JTextField txt_idCliente;
    public static javax.swing.JTextField txt_idReserva;
    public static javax.swing.JTextField txt_totalPagar;
    // End of variables declaration//GEN-END:variables
}
