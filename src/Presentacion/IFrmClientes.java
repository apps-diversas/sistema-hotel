package Presentacion;

import Dao.ClienteDao;
import Dao.EstadoDao;
import Dao.PaisDao;
import Datos.Cliente;
import Utilidades.Conexion;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.Date;
import java.util.Calendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

public class IFrmClientes extends javax.swing.JInternalFrame {
    
    public IFrmClientes() {
        initComponents();
        //this.getContentPane().setBackground(new Color(238, 232, 205)); //Color Royal
        //this.setIconifiable(true);
        inhabilitar();
        //OcultarColumnas();
        mostrarClientes("");
        cagarPais();
        cargarEstadoCliente();
    }
    
    private String accion = "guardar";
    Connection cn = Conexion.getConnection();
    
    void OcultarColumnas() {
        tbl_listado.getColumnModel().getColumn(2).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(2).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(2).setPreferredWidth(0);
        
        tbl_listado.getColumnModel().getColumn(3).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(3).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(3).setPreferredWidth(0);
        
        tbl_listado.getColumnModel().getColumn(4).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(4).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(4).setPreferredWidth(0);
        
        tbl_listado.getColumnModel().getColumn(9).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(9).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(9).setPreferredWidth(0);
        
        tbl_listado.getColumnModel().getColumn(10).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(10).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(10).setPreferredWidth(0);
        
        tbl_listado.getColumnModel().getColumn(11).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(11).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(11).setPreferredWidth(0);
        
        tbl_listado.getColumnModel().getColumn(13).setMaxWidth(0);
        tbl_listado.getColumnModel().getColumn(13).setMinWidth(0);
        tbl_listado.getColumnModel().getColumn(13).setPreferredWidth(0);
    }
    
    void inhabilitar() {
        
        accion = "guardar";
        txt_idCliente.setEnabled(false);
        txt_Dni.setEnabled(false);
        txt_Nombres.setEnabled(false);
        txt_APaterno.setEnabled(false);
        txt_AMaterno.setEnabled(false);
        dc_fecha_nac.setEnabled(false);
        txt_direccion.setEnabled(false);
        txt_telefono.setEnabled(false);
        txt_email.setEnabled(false);
        cbo_pais.setEnabled(false);
        cbo_estado.setEnabled(false);
        
        btn_guardar.setEnabled(false);
        btn_cancelar.setEnabled(false);
        btn_eliminar.setEnabled(false);
        btn_nuevo.setEnabled(true);
        
        txt_idCliente.setText("");
        txt_Dni.setText("");
        txt_Nombres.setText("");
        txt_APaterno.setText("");
        txt_AMaterno.setText("");
        //dc_fecha_nac.setDate(10-09-15);
        txt_direccion.setText("");
        txt_telefono.setText("");
        txt_email.setText("");
        //cbo_pais.setSelectedIndex(0);
        //cbo_estado.setSelectedIndex(0);
    }
    
    void habilitar() {
        txt_idCliente.setEnabled(true);
        txt_Dni.setEnabled(true);
        txt_Nombres.setEnabled(true);
        txt_APaterno.setEnabled(true);
        txt_AMaterno.setEnabled(true);
        dc_fecha_nac.setEnabled(true);
        txt_direccion.setEnabled(true);
        txt_telefono.setEnabled(true);
        txt_email.setEnabled(true);
        cbo_pais.setEnabled(true);
        cbo_estado.setEnabled(true);
        
        btn_guardar.setEnabled(true);
        btn_cancelar.setEnabled(true);
        //btn_eliminar.setEnabled(false);
        btn_nuevo.setEnabled(false);
        
        txt_idCliente.setText("");
        txt_Dni.setText("");
        txt_Nombres.setText("");
        txt_APaterno.setText("");
        txt_AMaterno.setText("");
        //dc_fecha_nac.setDate(Date.valueOf("15-10-2015"));
        //dc_fecha_nac.setDate(10-09-15);
        txt_direccion.setText("");
        txt_telefono.setText("");
        txt_email.setText("");
        //cbo_pais.setSelectedIndex(0);
        //cbo_estado.setSelectedIndex(0);
    }
    
    private void validarCasillas() {
        
        if (txt_idCliente.getText().length() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Debes ingresar un Código para el Cliente");
            txt_idCliente.requestFocus();
            return;
        }
        if (txt_Dni.getText().length() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Debes ingresar un DNI para el Cliente");
            txt_Dni.requestFocus();
            return;
        }
        if (txt_Nombres.getText().length() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Debes ingresar un Nombre para el Cliente");
            txt_Nombres.requestFocus();
            return;
        }
        if (txt_APaterno.getText().length() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Debes ingresar un Apellido Paterno para el Cliente");
            txt_APaterno.requestFocus();
            return;
        }
        if (txt_AMaterno.getText().length() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Debes ingresar un Apellido Materno para el Cliente");
            txt_AMaterno.requestFocus();
            return;
        }
        
        Calendar cal;
        int a;
        cal = dc_fecha_nac.getCalendar();
        a = 2015 - cal.get(Calendar.YEAR);
        if (a <= 18) {
            JOptionPane.showMessageDialog(rootPane, "El Cliente debe ser mayor de edad");
            dc_fecha_nac.requestFocus();
            return;
        }
        if (a >= 115) {
            JOptionPane.showMessageDialog(rootPane, "¿Es en serio?");
            dc_fecha_nac.requestFocus();
            return;
        }
        
        if (txt_direccion.getText().length() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Debes ingresar una Direccion para el Cliente");
            txt_direccion.requestFocus();
            return;
        }
        
    }
    
    private void mostrarClientes(String buscar) {
        
        try {
            DefaultTableModel modelo;
            ClienteDao cliDao = new ClienteDao();
            modelo = cliDao.mostrar(buscar);
            
            lbl_totalRegistros.setText("Nro. de Registros: " + Integer.toString(cliDao.totalRegistros));
            tbl_listado.setModel(modelo);
            OcultarColumnas();
            
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
        
    }
    
    public void cagarPais() {
        ClienteDao cliDao = new ClienteDao();
        cbo_pais.setModel(cliDao.mostrarPaisCliente());
    }
    
    public void cargarEstadoCliente() {
        ClienteDao cliDao = new ClienteDao();
        cbo_estado.setModel(cliDao.mostrarEstadoCliente());
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_listado = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        txt_buscar = new javax.swing.JTextField();
        btn_buscar = new javax.swing.JButton();
        btn_eliminar = new javax.swing.JButton();
        btn_salir = new javax.swing.JButton();
        lbl_totalRegistros = new javax.swing.JLabel();
        btn_imprimir = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txt_idCliente = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_Dni = new javax.swing.JTextField();
        cbo_pais = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        cbo_estado = new javax.swing.JComboBox();
        btn_nuevo = new javax.swing.JButton();
        btn_guardar = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_email = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txt_telefono = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_Nombres = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txt_APaterno = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txt_AMaterno = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        txt_direccion = new javax.swing.JTextArea();
        jLabel14 = new javax.swing.JLabel();
        dc_fecha_nac = new com.toedter.calendar.JDateChooser();
        jLabel15 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();

        jPanel2.setBackground(new java.awt.Color(235, 236, 228));

        tbl_listado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_listado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_listado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_listadoMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_listado);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("Nombre(s) / Apellido(s)");

        txt_buscar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_buscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_buscarKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_buscarKeyTyped(evt);
            }
        });

        btn_buscar.setBackground(new java.awt.Color(0, 0, 0));
        btn_buscar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_buscar.setForeground(new java.awt.Color(255, 255, 255));
        btn_buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/buscar.png"))); // NOI18N
        btn_buscar.setText("Buscar");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });

        btn_eliminar.setBackground(new java.awt.Color(0, 0, 0));
        btn_eliminar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_eliminar.setForeground(new java.awt.Color(255, 255, 255));
        btn_eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/eliminar.png"))); // NOI18N
        btn_eliminar.setText("Inactivar");
        btn_eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_eliminarActionPerformed(evt);
            }
        });

        btn_salir.setBackground(new java.awt.Color(0, 0, 0));
        btn_salir.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_salir.setForeground(new java.awt.Color(255, 255, 255));
        btn_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/home.png"))); // NOI18N
        btn_salir.setText("Salir");
        btn_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salirActionPerformed(evt);
            }
        });

        lbl_totalRegistros.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_totalRegistros.setText("Nro. de Registros");

        btn_imprimir.setBackground(new java.awt.Color(255, 51, 102));
        btn_imprimir.setForeground(new java.awt.Color(255, 255, 255));
        btn_imprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Print.png"))); // NOI18N
        btn_imprimir.setText("Imprimir");
        btn_imprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_imprimirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_imprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbl_totalRegistros, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(143, 143, 143)
                .addComponent(btn_buscar, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(btn_eliminar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_salir)
                .addGap(39, 39, 39))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txt_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_buscar)
                    .addComponent(btn_eliminar)
                    .addComponent(btn_salir))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_totalRegistros)
                    .addComponent(btn_imprimir))
                .addGap(13, 13, 13))
        );

        jLabel9.getAccessibleContext().setAccessibleName("");

        jPanel6.setBackground(new java.awt.Color(235, 236, 228));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Listado de clientes");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel6)
        );

        jPanel1.setBackground(new java.awt.Color(122, 197, 205));

        txt_idCliente.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_idCliente.setMaximumSize(new java.awt.Dimension(6, 6));
        txt_idCliente.setMinimumSize(new java.awt.Dimension(6, 6));
        txt_idCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_idClienteKeyTyped(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("DNI");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("País");

        txt_Dni.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_Dni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_DniKeyTyped(evt);
            }
        });

        cbo_pais.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Estado");

        cbo_estado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        btn_nuevo.setBackground(new java.awt.Color(0, 0, 0));
        btn_nuevo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_nuevo.setForeground(new java.awt.Color(255, 255, 255));
        btn_nuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/plus.png"))); // NOI18N
        btn_nuevo.setText("Nuevo");
        btn_nuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_nuevoActionPerformed(evt);
            }
        });

        btn_guardar.setBackground(new java.awt.Color(0, 0, 0));
        btn_guardar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_guardar.setForeground(new java.awt.Color(255, 255, 255));
        btn_guardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ok1.png"))); // NOI18N
        btn_guardar.setText("Guardar");
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });

        btn_cancelar.setBackground(new java.awt.Color(0, 0, 0));
        btn_cancelar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_cancelar.setForeground(new java.awt.Color(255, 255, 255));
        btn_cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/cancelar.png"))); // NOI18N
        btn_cancelar.setText("Cancelar");
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Código");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Email");

        txt_email.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_email.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_emailKeyTyped(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setText("Teléfono");

        txt_telefono.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_telefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_telefonoKeyTyped(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setText("Nombre(s)");

        txt_Nombres.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_Nombres.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_NombresKeyTyped(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("A. Paterno");

        txt_APaterno.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_APaterno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_APaternoKeyTyped(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel13.setText("A. Materno");

        txt_AMaterno.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_AMaterno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_AMaternoKeyTyped(evt);
            }
        });

        txt_direccion.setColumns(20);
        txt_direccion.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_direccion.setRows(5);
        txt_direccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_direccionKeyTyped(evt);
            }
        });
        jScrollPane4.setViewportView(txt_direccion);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel14.setText("Dirección");

        dc_fecha_nac.setDateFormatString("dd/MM/yyyy");
        dc_fecha_nac.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        dc_fecha_nac.setMinSelectableDate(new java.util.Date(-62135747920000L));
        dc_fecha_nac.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                dc_fecha_nacKeyTyped(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel15.setText("F. Nacimiento");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_nuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_guardar, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_cancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel2)
                            .addComponent(jLabel8)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13)
                            .addComponent(jLabel15)
                            .addComponent(jLabel14)
                            .addComponent(jLabel11)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_Dni, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(dc_fecha_nac, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                                .addComponent(txt_AMaterno, javax.swing.GroupLayout.Alignment.LEADING))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cbo_estado, 0, 187, Short.MAX_VALUE)
                                    .addComponent(txt_idCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_email)
                                    .addComponent(txt_telefono, javax.swing.GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                                    .addComponent(txt_Nombres, javax.swing.GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                                    .addComponent(txt_APaterno, javax.swing.GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                                    .addComponent(cbo_pais, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(55, 55, 55))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel10)
                    .addComponent(txt_idCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel2)
                    .addComponent(txt_Dni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txt_Nombres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_APaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txt_AMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel15)
                    .addComponent(dc_fecha_nac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addGap(60, 60, 60))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_telefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txt_email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbo_pais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbo_estado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_guardar)
                    .addComponent(btn_nuevo)
                    .addComponent(btn_cancelar))
                .addGap(15, 15, 15))
        );

        jPanel3.setBackground(new java.awt.Color(79, 79, 79));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Clientes");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(122, 197, 205));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Registro de clientes");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel5)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(15, 15, 15))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(36, 36, 36))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tbl_listadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_listadoMouseClicked
        
        btn_guardar.setText("Editar");
        habilitar();
        accion = "editar";
        int fila = tbl_listado.rowAtPoint(evt.getPoint());
        
        txt_idCliente.setText(tbl_listado.getValueAt(fila, 0).toString());
        txt_Dni.setText(tbl_listado.getValueAt(fila, 1).toString());
        txt_Nombres.setText(tbl_listado.getValueAt(fila, 2).toString());
        txt_APaterno.setText(tbl_listado.getValueAt(fila, 3).toString());
        txt_AMaterno.setText(tbl_listado.getValueAt(fila, 4).toString());
        dc_fecha_nac.setDate(Date.valueOf(tbl_listado.getValueAt(fila, 6).toString()));
        txt_direccion.setText(tbl_listado.getValueAt(fila, 8).toString());
        txt_telefono.setText(tbl_listado.getValueAt(fila, 9).toString());
        txt_email.setText(tbl_listado.getValueAt(fila, 10).toString());
        
        cbo_pais.setSelectedItem(tbl_listado.getValueAt(fila, 12).toString());
        cbo_estado.setSelectedItem(tbl_listado.getValueAt(fila, 14).toString());
        
        txt_idCliente.setEnabled(false);
        btn_eliminar.setEnabled(true);

    }//GEN-LAST:event_tbl_listadoMouseClicked

    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed
        
        mostrarClientes(txt_buscar.getText());
    }//GEN-LAST:event_btn_buscarActionPerformed

    private void btn_eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_eliminarActionPerformed
        
        if (!txt_idCliente.getText().equals("")) {
            int confirmacion = JOptionPane.showConfirmDialog(rootPane, "¿Estás seguro que desea Elimnar el Cliente?", "Eliminar", JOptionPane.YES_NO_OPTION);
            
            if (confirmacion == 0) {
                ClienteDao cliDao = new ClienteDao();
                Cliente cli = new Cliente();
                
                cli.setIdCliente(txt_idCliente.getText());
                cliDao.eliminar(cli);
                JOptionPane.showConfirmDialog(rootPane, "Cliente eliminado con éxito", "Eliminar", JOptionPane.YES_NO_OPTION);
                mostrarClientes("");
                inhabilitar();
            }
            
        }
    }//GEN-LAST:event_btn_eliminarActionPerformed

    private void btn_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btn_salirActionPerformed

    private void txt_idClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_idClienteKeyTyped
        int limite = 6;
        if (txt_idCliente.getText().length() == limite) {
            evt.consume();
        }
    }//GEN-LAST:event_txt_idClienteKeyTyped

    private void txt_DniKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_DniKeyTyped
        
        char c = evt.getKeyChar();
        int limite = 8;
        
        if (Character.isLetter(c)) {
            getToolkit().beep();
            
            evt.consume();
            
            JOptionPane.showMessageDialog(rootPane, "Ingresa solo Numeros", "Mensaje", WIDTH, null);
            //Error.setText("Ingresa Solo Numeros");
        } else {
            if (txt_Dni.getText().length() == limite) {
                evt.consume();
            }
        }
    }//GEN-LAST:event_txt_DniKeyTyped

    private void btn_nuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_nuevoActionPerformed
        habilitar();
    }//GEN-LAST:event_btn_nuevoActionPerformed

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed
        
        ClienteDao cliDao = new ClienteDao();
        validarCasillas();
        
//        Pais pais = (Pais) cbo_pais.getSelectedItem();
//        Estado estado = (Estado) cbo_estado.getSelectedItem();
        
        Cliente cli = new Cliente();
        
        cli.setIdCliente(txt_idCliente.getText());
        cli.setDni(txt_Dni.getText());
        cli.setNombres(txt_Nombres.getText());
        cli.setApaterno(txt_APaterno.getText());
        cli.setAmaterno(txt_AMaterno.getText());
        
        Calendar cal;
        int d, m, a;
        cal = dc_fecha_nac.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR)-1900;

        cli.setFecha_nacimiento(new Date(a, m, d));
        
        cli.setDireccion(txt_direccion.getText());
        cli.setTelefono(txt_telefono.getText());
        cli.setEmail(txt_email.getText());
        
        PaisDao p = new PaisDao();
        String idPais = p.capturarIdPais(cbo_pais.getSelectedItem().toString());
        cli.setPais(idPais);
        
        EstadoDao e = new EstadoDao();
        String idEstado = e.capturarIdEstado(cbo_estado.getSelectedItem().toString());
        cli.setEstado(idEstado);
        
        if ("guardar".equals(accion)) {
            
            int rpta = JOptionPane.showConfirmDialog(null, "¿Seguro que desea registrar el cliente?", "Registrar", JOptionPane.YES_NO_OPTION);
            if (rpta == JOptionPane.YES_OPTION) {
                cliDao.insertar(cli);
                JOptionPane.showMessageDialog(rootPane, "Cliente registrado con éxito", "Registrar", WIDTH, null);
                mostrarClientes("");
                inhabilitar();
            }
        }
        
        if ("editar".equals(accion)) {
            
            int rpta = JOptionPane.showConfirmDialog(null, "¿Seguro que desea modificar el cliente?", "Modificar", JOptionPane.YES_NO_OPTION);
            if (rpta == JOptionPane.YES_OPTION) {
                
                cliDao.editar(cli);
                
                JOptionPane.showMessageDialog(rootPane, "Cliente modificado con éxito", "Modificar", WIDTH, null);
                mostrarClientes("");
                inhabilitar();
                btn_guardar.setText("Guardar");
                btn_eliminar.setEnabled(false);
            }
        }
    }//GEN-LAST:event_btn_guardarActionPerformed

    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        inhabilitar();
        accion = "guardar";
        btn_guardar.setText("Guardar");
    }//GEN-LAST:event_btn_cancelarActionPerformed

    private void txt_emailKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_emailKeyTyped
        int limite = 50;
        if (txt_direccion.getText().length() == limite) {
            evt.consume();
        }
    }//GEN-LAST:event_txt_emailKeyTyped

    private void txt_telefonoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_telefonoKeyTyped
        
        char c = evt.getKeyChar();
        int limite = 12;
        
        if (Character.isLetter(c)) {
            getToolkit().beep();
            
            evt.consume();
            
            JOptionPane.showMessageDialog(rootPane, "Ingresa solo Numeros", "Mensaje", WIDTH, null);
            //Error.setText("Ingresa Solo Numeros");
        } else {
            if (txt_telefono.getText().length() == limite) {
                evt.consume();
            }
        }

    }//GEN-LAST:event_txt_telefonoKeyTyped

    private void txt_NombresKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_NombresKeyTyped
        
        char c = evt.getKeyChar();
        int limite = 40;
        
        if (Character.isDigit(c)) {
            getToolkit().beep();
            
            evt.consume();
            
            JOptionPane.showMessageDialog(rootPane, "Ingresa solo Letras", "Mensaje", WIDTH, null);
            //Error.setText("Ingresa Solo Numeros");
        } else {
            if (txt_Nombres.getText().length() == limite) {
                evt.consume();
            }
        }

    }//GEN-LAST:event_txt_NombresKeyTyped

    private void txt_APaternoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_APaternoKeyTyped
        char c = evt.getKeyChar();
        int limite = 20;
        
        if (Character.isDigit(c)) {
            getToolkit().beep();
            
            evt.consume();
            
            JOptionPane.showMessageDialog(rootPane, "Ingresa solo Letras", "Mensaje", WIDTH, null);
            //Error.setText("Ingresa Solo Numeros");
        } else {
            if (txt_APaterno.getText().length() == limite) {
                evt.consume();
            }
        }
    }//GEN-LAST:event_txt_APaternoKeyTyped

    private void txt_AMaternoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_AMaternoKeyTyped
        char c = evt.getKeyChar();
        int limite = 20;
        
        if (Character.isDigit(c)) {
            getToolkit().beep();
            
            evt.consume();
            
            JOptionPane.showMessageDialog(rootPane, "Ingresa solo Letras", "Mensaje", WIDTH, null);
            //Error.setText("Ingresa Solo Numeros");
        } else {
            if (txt_AMaterno.getText().length() == limite) {
                evt.consume();
            }
        }
    }//GEN-LAST:event_txt_AMaternoKeyTyped

    private void txt_direccionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_direccionKeyTyped
        
        int limite = 100;
        if (txt_direccion.getText().length() == limite) {
            evt.consume();
        }
    }//GEN-LAST:event_txt_direccionKeyTyped

    private void txt_buscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_buscarKeyPressed
        
     
        
    }//GEN-LAST:event_txt_buscarKeyPressed

    private void dc_fecha_nacKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dc_fecha_nacKeyTyped

        
    }//GEN-LAST:event_dc_fecha_nacKeyTyped

    private void txt_buscarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_buscarKeyTyped

        char c = evt.getKeyChar();
        int limite = 20;
        
        if (Character.isDigit(c)) {
            getToolkit().beep();
            
            evt.consume();
            
            JOptionPane.showMessageDialog(rootPane, "Ingresa solo Letras", "Mensaje", WIDTH, null);
            //Error.setText("Ingresa Solo Numeros");
        } else {
            if (txt_buscar.getText().length() == limite) {
                evt.consume();
            }
        }
        
    }//GEN-LAST:event_txt_buscarKeyTyped

    private void btn_imprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_imprimirActionPerformed

        try {
            String ruta = "src\\Reportes\\rCliente.jrxml";
            JasperReport rep = JasperCompileManager.compileReport(ruta);

            JasperPrint reporte = JasperFillManager.fillReport(rep, null, cn);
            JasperViewer visor = new JasperViewer(reporte, false);
            visor.setTitle("Clientes");
            visor.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPane, "Error"+ e.toString());
        }
    }//GEN-LAST:event_btn_imprimirActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_buscar;
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_eliminar;
    private javax.swing.JButton btn_guardar;
    private javax.swing.JButton btn_imprimir;
    private javax.swing.JButton btn_nuevo;
    private javax.swing.JButton btn_salir;
    private javax.swing.JComboBox cbo_estado;
    private javax.swing.JComboBox cbo_pais;
    private com.toedter.calendar.JDateChooser dc_fecha_nac;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lbl_totalRegistros;
    private javax.swing.JTable tbl_listado;
    private javax.swing.JTextField txt_AMaterno;
    private javax.swing.JTextField txt_APaterno;
    private javax.swing.JTextField txt_Dni;
    private javax.swing.JTextField txt_Nombres;
    private javax.swing.JTextField txt_buscar;
    private javax.swing.JTextArea txt_direccion;
    private javax.swing.JTextField txt_email;
    private javax.swing.JTextField txt_idCliente;
    private javax.swing.JTextField txt_telefono;
    // End of variables declaration//GEN-END:variables

}
