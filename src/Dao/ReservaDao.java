package Dao;

import Datos.Reserva;
import Interfaces.IReserva;
import Utilidades.Conexion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

public class ReservaDao implements IReserva {

    Connection cn = Conexion.getConnection();
    String sql = "";
    public Reserva resConsulta = new Reserva();
    public String consultaIdEstado = "";
    public String consultaDesEstado = "";
    //public Estado estConsulta = new Estado();
    public String clienteDes = "";

    @Override
    public boolean insertar(Reserva res) {
        sql = "INSERT INTO RESERVA "
                + " VALUES (?,?,?,'0.00',?,?,'0','0.00')";
        try {
            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, res.getIdReserva());
            pst.setString(2, res.getIdCliente());
            pst.setString(3, res.getFechaRegistro());
            //pst.setDouble(4, hab.getTotal());
            pst.setString(4, res.getIdEstado());
            pst.setString(5, res.getIdEmpleado());
            //pst.setString(7, hab.getFlagConfirma());
            //pst.setDouble(8, hab.getTotalConfirma());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }
    }

    @Override
    public boolean editar(Reserva res) {
        sql = "UPDATE RESERVA SET ID_CLIENTE=?,FECHA=?,TOTAL=?,"
                + "ID_ESTADO=?,ID_EMPLEADO=?,FLAG_CONFIRMA=?,TOTAL_CONFIRMA=? WHERE ID_RESERVA=?";

        try {
            PreparedStatement pst = cn.prepareStatement(sql);

            pst.setString(1, res.getIdCliente());
            pst.setString(2, res.getFechaRegistro());
            pst.setDouble(3, res.getTotal());
            pst.setString(4, res.getIdEstado());
            pst.setString(5, res.getIdEmpleado());
            pst.setString(6, res.getFlagConfirma());
            pst.setDouble(7, res.getTotalConfirma());

            pst.setString(8, res.getIdReserva());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }
    }

    @Override
    public boolean eliminar(Reserva res) {
        sql = "UPDATE RESERVA SET ID_ESTADO='21' WHERE ID_RESERVA=?";

        try {
            PreparedStatement pst = cn.prepareStatement(sql);

            pst.setString(1, res.getIdReserva());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }
    }
    
    @Override
    public boolean eliminarDetalle(Reserva res) {
        sql = "UPDATE DETALLE_RESERVA SET ID_ESTADO='02' WHERE ID_RESERVA=?";

        try {
            PreparedStatement pst = cn.prepareStatement(sql);

            pst.setString(1, res.getIdReserva());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }
    }

    @Override
    public DefaultComboBoxModel mostrarEstadoInicio() {
        sql = "SELECT DESCRIPCION FROM ESTADO WHERE ID_ESTADO IN ('18','19','20','21','22')";
        DefaultComboBoxModel cbox = new DefaultComboBoxModel();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cbox.addElement(rs.getString("DESCRIPCION"));
            }

        } catch (Exception e) {
        }

        return cbox;
    }

    @Override
    public boolean consultar(String idReserva) {
        sql = "SELECT ID_RESERVA,R.ID_CLIENTE,FECHA,TOTAL,R.ID_ESTADO,ID_EMPLEADO,FLAG_CONFIRMA,TOTAL_CONFIRMA,"
                + "R.ID_ESTADO,E.DESCRIPCION,C.NOMBRE+', '+C.APEPATERNO "
                + " FROM RESERVA R"
                + " INNER JOIN ESTADO E ON E.ID_ESTADO=R.ID_ESTADO"
                + " INNER JOIN CLIENTE C ON C.ID_CLIENTE=R.ID_CLIENTE"
                + " WHERE R.ID_RESERVA='" + idReserva + "'";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                resConsulta.setIdReserva(rs.getString(1));
                resConsulta.setIdCliente(rs.getString(2));
                resConsulta.setFechaRegistro(rs.getString(3));
                resConsulta.setTotal(Double.parseDouble(rs.getString(4)));
                resConsulta.setIdEstado(rs.getString(5));
                resConsulta.setIdEmpleado(rs.getString(6));
                resConsulta.setFlagConfirma(rs.getString(7));
                resConsulta.setTotalConfirma(Double.parseDouble(rs.getString(8)));

                consultaIdEstado = rs.getString(9);
                consultaDesEstado = rs.getString(10);
                clienteDes = (rs.getString(11));
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public String capturaCodigoCliente(String idReserva) {
        sql = "SELECT ID_CLIENTE FROM RESERVA WHERE ID_RESERVA='" + idReserva + "'";
        String idCliente2 = "";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                idCliente2 = rs.getString(1);
            }

        } catch (Exception e) {
        }

        return idCliente2;

    }

    @Override
    public double totalPagarDetalle(String idReserva) {

        sql = "SELECT (SELECT SUM(IMPORTE)-SUM(DESCUENTO) FROM DETALLE_RESERVA WHERE ID_RESERVA=R.ID_RESERVA AND ID_ESTADO='01')"
                + "FROM RESERVA R WHERE R.ID_RESERVA='" + idReserva + "'";
        double totalPagar = 0.00;

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                totalPagar = rs.getDouble(1);
            }

        } catch (Exception e) {
        }

        return totalPagar;

    }

    @Override
    public boolean finalizarReserva(String idReserva) {
        
        sql = "UPDATE RESERVA SET ID_ESTADO='22' WHERE ID_RESERVA=?";

        try {
            PreparedStatement pst = cn.prepareStatement(sql);

            pst.setString(1, idReserva);

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }
        
    }

    @Override
    public int PagosPendientes(String idReserva) {
        
        sql = "SELECT COUNT(*) FROM PAGO WHERE ID_RESERVA='"+idReserva+"' AND FLAG_CONFIRMA='0'";
        int resultado = 0;
        
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                resultado= rs.getInt(1);
            } else {
                return 0;
            }
            return resultado;
        } catch (Exception e) {
            return 0;
        }
        
    }
    
}
