
package Dao;

import Utilidades.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class PaisDao {

    Connection cn = Conexion.getConnection();
    String sql = "";
    
    public String capturarIdPais(String descripcion) {

        sql = "SELECT ID_PAIS FROM PAIS WHERE DESCRIPCION='"+descripcion+"'";
        String idPais = "";
        
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            if (rs.next()==true) {                
                idPais = rs.getString("ID_PAIS");
            }
            
        } catch (Exception e) {
        }

        return idPais;
    }
    
}
