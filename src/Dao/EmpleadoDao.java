package Dao;

import Datos.Empleado;
import Interfaces.IEmpleado;
import Utilidades.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class EmpleadoDao implements IEmpleado {

    Connection cn = Conexion.getConnection();
    String sql = "";
    public Integer totalRegistros = 0;
    public Integer n = 0;

    @Override
    public DefaultTableModel mostrar(String buscar) {

        DefaultTableModel modelo = null;

        String[] titulos = {"ID", "DNI", "Empleado", "Edad", "Direccion", "Telefono", "Email", "Pais", "Sueldo", "Estado"};
        String[] registros = new String[10];

        modelo = new DefaultTableModel(null, titulos);
        sql = "SELECT E.ID_EMPLEADO, E.DNI, E.APEPATERNO+' '+E.APEMATERNO+', '+E.NOMBRE,"
                + "DATEDIFF(yy,E.FECHA_NACIMIENTO,GETDATE()),E.DIRECCION,E.TELEFONO,E.EMAIL,P.DESCRIPCION,E.SUELDO,ES.DESCRIPCION "
                + "FROM EMPLEADO E "
                + "INNER JOIN PAIS P ON P.ID_PAIS=E.ID_PAIS "
                + "INNER JOIN ESTADO ES ON ES.ID_ESTADO=E.ID_ESTADO "
                + "WHERE (E.APEPATERNO+' '+E.APEMATERNO+', '+E.NOMBRE) like '%" + buscar + "%' ORDER BY E.ID_EMPLEADO";

        try {

            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                registros[0] = rs.getString(1);
                registros[1] = rs.getString(2);
                registros[2] = rs.getString(3);
                registros[3] = rs.getString(4);
                registros[4] = rs.getString(5);
                registros[5] = rs.getString(6);
                registros[6] = rs.getString(7);
                registros[7] = rs.getString(8);
                registros[8] = rs.getString(9);
                registros[9] = rs.getString(10);
                registros[10] = rs.getString(11);

                totalRegistros = totalRegistros + 1;
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;
        }

    }

    @Override
    public DefaultComboBoxModel mostrarPais() {
        sql = "SELECT * FROM PAIS";
        DefaultComboBoxModel cbox = new DefaultComboBoxModel();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cbox.addElement(rs.getString("DESCRIPCION"));
            }

        } catch (Exception e) {
        }

        return cbox;
    }

    @Override
    public DefaultComboBoxModel mostrarEstadoEmpleado() {
        sql = "SELECT * FROM ESTADO WHERE ID_ESTADO IN ('01','02')";
        DefaultComboBoxModel cbox = new DefaultComboBoxModel();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cbox.addElement(rs.getString("DESCRIPCION"));
            }

        } catch (Exception e) {
        }

        return cbox;
    }

    @Override
    public boolean insertar(Empleado emp) {
        sql = "INSERT INTO habitacion "
                + " VALUES (?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, emp.getIdEmpleado());
            pst.setString(2, emp.getDni());
            pst.setString(3, emp.getNombre());
            pst.setString(4, emp.getApe_paterno());
            pst.setString(5, emp.getApe_materno());
            pst.setDate(6, emp.getFec_nacimiento());
            pst.setString(7, emp.getDireccion());
            pst.setString(8, emp.getTelefono());
            pst.setString(9, emp.getEmail());
            pst.setString(10, emp.getPais());
            pst.setDouble(11, emp.getSueldo());
            pst.setString(12, emp.getEstado());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }
    }

    @Override
    public boolean editar(Empleado emp) {
        sql = "UPDATE EMPLEADO SET DNI=?, NOMBRE=?, APEPATERNO=?, APEMATERNO=?, FECHA_NACIMIENTO=?, DIRECCION=?, TELEFONO=?," +
                "EMAIL=?, ID_PAIS=?, SUELDO=?, ID_ESTADO=?"
                + "WHERE ID_EMPLEADO=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, emp.getDni());
            pst.setString(2, emp.getNombre());
            pst.setString(3, emp.getApe_paterno());
            pst.setString(4, emp.getApe_materno());
            pst.setDate(5, emp.getFec_nacimiento());
            pst.setString(6, emp.getDireccion());
            pst.setString(7, emp.getTelefono());
            pst.setString(8, emp.getEmail());
            pst.setString(9, emp.getPais());
            pst.setDouble(10, emp.getSueldo());
            pst.setString(11, emp.getEstado());
            
            pst.setString(12, emp.getIdEmpleado());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }
    }

    @Override
    public boolean eliminar(Empleado emp) {
        sql = "DELETE FROM EMPLEADO WHERE ID_EMPLEADO=?";

        try {

            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, emp.getIdEmpleado());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }
    }

    @Override
    public Empleado login(String usuario, String password) {

        Empleado emp = new Empleado();
        sql = "SELECT L.ID_EMPLEADO,E.DNI,E.NOMBRE,E.APEPATERNO,E.APEMATERNO,A.DESCRIPCION "
                + "FROM LOGIN L "
                + "INNER JOIN EMPLEADO E ON E.ID_EMPLEADO=L.ID_EMPLEADO "
                + "INNER JOIN ACCESO A ON A.ID_ACCESO=L.ID_ACCESO "
                + "WHERE L.USUARIO='" + usuario + "' and L.PASSWORD='" + password + "'";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            
            while (rs.next()) {
                emp.setIdEmpleado(rs.getString(1));
                emp.setDni(rs.getString(2));
                emp.setNombre(rs.getString(3));
                emp.setApe_paterno(rs.getString(4));
                emp.setApe_materno(rs.getString(5));
                emp.setAcceso(rs.getString(6));
                n++;
            }
            
        } catch (Exception e) {
             JOptionPane.showConfirmDialog(null, e);
        }
        return emp;
    }

}
