package Dao;

import Datos.Pago;
import Interfaces.IPago;
import Utilidades.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class PagoDao implements IPago {

    Connection cn = Conexion.getConnection();
    String sql = "";
    public Pago pagoConsulta = new Pago();

    @Override
    public boolean insertar(Pago pago) {

        sql = "INSERT INTO PAGO "
                + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sql);

            pst.setString(1, pago.getIdPago());
            pst.setInt(2, pago.getNroPago());
            pst.setInt(3, pago.getNroComprobante());
            pst.setString(4, pago.getFechaEmision());
            pst.setDouble(5, pago.getSubTotal());
            pst.setDouble(6, pago.getIgv());
            pst.setDouble(7, pago.getTotal());
            pst.setString(8, pago.getIdReserva());
            pst.setString(9, pago.getIdTipoPago());
            pst.setString(10, pago.getIdTipoComp());
            pst.setString(11, pago.getIdEstado());
            pst.setString(12, pago.getFlag_confirma());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }

    }

    @Override
    public boolean eliminar(Pago pago) {

        sql = "UPDATE PAGO SET ID_ESTADO='04' WHERE ID_PAGO=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sql);

            pst.setString(1, pago.getIdPago());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }

    }

    @Override
    public boolean consultar(String idPago) {

        sql = "SELECT * FROM PAGO WHERE ID_PAGO='" + idPago + "'";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                pagoConsulta.setIdPago(rs.getString(1));
                pagoConsulta.setNroPago(Integer.parseInt(rs.getString(2)));
                pagoConsulta.setNroComprobante(Integer.parseInt(rs.getString(3)));
                pagoConsulta.setFechaEmision(rs.getString(4));
                pagoConsulta.setSubTotal(Double.parseDouble(rs.getString(5)));
                pagoConsulta.setIgv(Double.parseDouble(rs.getString(6)));
                pagoConsulta.setTotal(Double.parseDouble(rs.getString(7)));
                pagoConsulta.setIdReserva(rs.getString(8));
                pagoConsulta.setIdTipoPago(rs.getString(9));
                pagoConsulta.setIdTipoComp(rs.getString(10));
                pagoConsulta.setIdEstado(rs.getString(11));
                pagoConsulta.setFlag_confirma(rs.getString(12));

                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public DefaultComboBoxModel mostrarEstadoInicio() {
        
        sql = "SELECT * FROM ESTADO WHERE ID_ESTADO IN ('03','04')";
        DefaultComboBoxModel cbox = new DefaultComboBoxModel();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cbox.addElement(rs.getString("DESCRIPCION"));
            }

        } catch (Exception e) {
        }

        return cbox;
        
    }

    @Override
    public DefaultComboBoxModel mostrarTipoComprobante() {
        sql = "SELECT * FROM TIPO_COMPROBANTE";
        DefaultComboBoxModel cbox = new DefaultComboBoxModel();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cbox.addElement(rs.getString("DESCRIPCION"));
            }

        } catch (Exception e) {
        }

        return cbox;
    }

    @Override
    public DefaultComboBoxModel mostrarTipoPago() {
        sql = "SELECT * FROM TIPO_PAGO";
        DefaultComboBoxModel cbox = new DefaultComboBoxModel();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cbox.addElement(rs.getString("DESCRIPCION"));
            }

        } catch (Exception e) {
        }

        return cbox;
    }

    @Override
    public DefaultTableModel mostrarPagado(String idReserva) {

        DefaultTableModel modelo;

        String[] titulos = {"Descripción", "Total"};
        String[] registros = new String[2];

        modelo = new DefaultTableModel(null, titulos);
        sql = "SELECT CASE WHEN FLAG_CONFIRMA='1' then 'CONFIRMA' else 'TOTAL' END,TOTAL "
                + "FROM PAGO "
                + "WHERE ID_RESERVA='" + idReserva + "' AND ID_ESTADO='03'";

        try {

            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                registros[0] = rs.getString(1);
                registros[1] = String.valueOf(rs.getDouble(2));

                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;
        }

    }

    @Override
    public DefaultTableModel mostrarPendiente(String idReserva) {
        DefaultTableModel modelo;

        String[] titulos = {"Descripción", "Total"};
        String[] registros = new String[2];

        modelo = new DefaultTableModel(null, titulos);
        sql = "SELECT 'CONFIRMA',TOTAL_CONFIRMA "
                + "FROM RESERVA R "
                + "WHERE ID_RESERVA='"+idReserva+"' AND "
                + "NOT EXISTS (SELECT * FROM PAGO WHERE ID_RESERVA='"+idReserva+"' AND TOTAL=R.TOTAL_CONFIRMA AND ID_ESTADO='03') "
                + "UNION "
                + "SELECT 'TOTAL',TOTAL-TOTAL_CONFIRMA "
                + "FROM RESERVA R "
                + "WHERE ID_RESERVA='"+idReserva+"' AND "
                + "NOT EXISTS (SELECT * FROM PAGO WHERE ID_RESERVA='"+idReserva+"' AND TOTAl=R.TOTAL-R.TOTAL_CONFIRMA AND ID_ESTADO='03')";

        try {

            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                registros[0] = rs.getString(1);
                registros[1] = String.valueOf(rs.getDouble(2));

                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;
        }
    }

}
