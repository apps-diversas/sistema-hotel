package Dao;

import Datos.DetalleReserva;
import Interfaces.IDetalleReserva;
import Utilidades.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class DetalleReservaDao implements IDetalleReserva {

    Connection cn = Conexion.getConnection();
    String sql = "";
    public Integer totalRegistros = 0;

    @Override
    public boolean insertar(DetalleReserva detRes) {
        
        sql = "INSERT INTO DETALLE_RESERVA "
                + " VALUES (?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, detRes.getId_reserva());
            pst.setInt(2, detRes.getItem());
            pst.setString(3, detRes.getId_habitacion());
            pst.setString(4, detRes.getFecha_ingreso());
            pst.setString(5, detRes.getFecha_salida());
            pst.setDouble(6, detRes.getImporte());
            pst.setDouble(7, detRes.getDescuento());
            pst.setString(8, detRes.getId_estado());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }
        
    }

    @Override
    public boolean editar(DetalleReserva detRes) {
        
        sql = "UPDATE DETALLE_RESERVA SET ID_HABITACION=?,FECHA_INGRESO=?,FECHA_SALIDA=?,"
                + "IMPORTE=?,DESCUENTO=?,ID_ESTADO=? WHERE ID_RESERVA=? AND ITEM=?";
        
        try {
            PreparedStatement pst = cn.prepareStatement(sql);
            
            pst.setString(1, detRes.getId_habitacion());
            pst.setString(2, detRes.getFecha_ingreso());
            pst.setString(3, detRes.getFecha_salida());
            pst.setDouble(4, detRes.getImporte());
            pst.setDouble(5, detRes.getDescuento());
            pst.setString(6, detRes.getId_estado());

            pst.setString(7, detRes.getId_reserva());
            pst.setInt(8, detRes.getItem());
            
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }
        
    }

    @Override
    public boolean eliminar(DetalleReserva detRes) {
        sql = "UPDATE DETALLE_RESERVA SET ID_ESTADO='02' "
                + "WHERE ID_RESERVA=? AND ITEM=?";

        try {
            PreparedStatement pst = cn.prepareStatement(sql);

            pst.setString(1, detRes.getId_reserva());
            pst.setInt(2, detRes.getItem());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }
    }

    @Override
    public DefaultComboBoxModel mostrarEstadoInicio() {
        sql = "SELECT DESCRIPCION FROM ESTADO WHERE ID_ESTADO IN ('01','02')";
        DefaultComboBoxModel cbox = new DefaultComboBoxModel();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cbox.addElement(rs.getString("DESCRIPCION"));
            }

        } catch (Exception e) {
        }

        return cbox;
    }

    @Override
    public DefaultTableModel mostrar(String id_reserva, int item) {

        DefaultTableModel modelo = null;

        String[] titulos = {"ID", "Item", "Número", "F.Ingreso", "F.Salida", "Importe", "Descuento", "Estado"};
        String[] registros = new String[8];

        modelo = new DefaultTableModel(null, titulos);

        if (item > 0) {
            sql = "SELECT RDET.ID_RESERVA,RDET.ITEM,H.NUMERO,RDET.FECHA_INGRESO,RDET.FECHA_SALIDA,RDET.IMPORTE,RDET.DESCUENTO,E.DESCRIPCION "
                    + "FROM DETALLE_RESERVA RDET "
                    + "INNER JOIN HABITACION H ON H.ID_HABITACION=RDET.ID_HABITACION "
                    + "INNER JOIN ESTADO E ON E.ID_ESTADO=RDET.ID_ESTADO "
                    + "WHERE RDET.ID_RESERVA='" + id_reserva + "' AND RDET.ITEM=" + String.valueOf(item) + " ORDER BY RDET.ITEM";
        } else {
            sql = "SELECT RDET.ID_RESERVA,RDET.ITEM,H.NUMERO,RDET.FECHA_INGRESO,RDET.FECHA_SALIDA,RDET.IMPORTE,RDET.DESCUENTO,E.DESCRIPCION "
                    + "FROM DETALLE_RESERVA RDET "
                    + "INNER JOIN HABITACION H ON H.ID_HABITACION=RDET.ID_HABITACION "
                    + "INNER JOIN ESTADO E ON E.ID_ESTADO=RDET.ID_ESTADO "
                    + "WHERE RDET.ID_RESERVA='" + id_reserva + "' ORDER BY RDET.ITEM";
        }

        try {

            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                registros[0] = rs.getString(1);
                registros[1] = rs.getString(2);
                registros[2] = rs.getString(3);
                registros[3] = rs.getString(4);
                registros[4] = rs.getString(5);
                registros[5] = rs.getString(6);
                registros[6] = rs.getString(7);
                registros[7] = rs.getString(8);

                totalRegistros = totalRegistros + 1;
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;
        }

    }

    @Override
    public int hallarItem(String id_reserva) {

        sql = "SELECT TOP 1 ITEM+1  "
                + "FROM DETALLE_RESERVA "
                + "WHERE ID_RESERVA='" + id_reserva + "' "
                + "ORDER BY ITEM DESC";

        int item = 0;

        try {

            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                item = rs.getInt(1);
            } else {
                item = 1;
            }
            return item;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return item;
        }

    }

    @Override
    public boolean verificar_disponibilidad(String fecha_ingreso, String fecha_salida, String numHab) {
        sql = "SELECT dbo.f_valida_disponibilidad('"+fecha_ingreso+"','"+fecha_salida+"','"+numHab+"')";
        boolean check;
        
        try {

            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                check=rs.getBoolean(1);
                return check;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }
    }

}
