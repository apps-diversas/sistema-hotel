
package Dao;

import Utilidades.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class EstadoDao {

    Connection cn = Conexion.getConnection();
    String sql = "";
    
    public String capturarIdEstado(String descripcion) {

        sql = "SELECT ID_ESTADO FROM ESTADO WHERE DESCRIPCION='"+descripcion+"'";
        String idEstado = "";
        
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            if (rs.next()==true) {                
                idEstado = rs.getString("ID_ESTADO");
            }
            
        } catch (Exception e) {
        }

        return idEstado;
    }
    
    public String capturarDescripcion(String idEstado) {

        sql = "SELECT DESCRIPCION FROM ESTADO WHERE ID_ESTADO='"+idEstado+"'";
        String descripcion = "";
        
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            if (rs.next()==true) {                
                descripcion = rs.getString("DESCRIPCION");
            }
            
        } catch (Exception e) {
        }

        return descripcion;
    }
    
}
