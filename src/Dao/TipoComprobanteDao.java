
package Dao;

import Utilidades.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class TipoComprobanteDao {
    
    Connection cn = Conexion.getConnection();
    String sql = "";

    public String capturarIdEstado(String descripcion) {

        sql = "SELECT ID_TIPO_COMPROBANTE FROM TIPO_COMPROBANTE WHERE DESCRIPCION='" + descripcion + "'";
        String idTipoC = "";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                idTipoC = rs.getString("ID_TIPO_COMPROBANTE");
            }

        } catch (Exception e) {
        }

        return idTipoC;
    }

    public String capturarDescripcion(String idTipoC) {

        sql = "SELECT DESCRIPCION FROM TIPO_COMPROBANTE WHERE ID_TIPO_COMPROBANTE='" + idTipoC + "'";
        String descripcion = "";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                descripcion = rs.getString("DESCRIPCION");
            }

        } catch (Exception e) {
        }

        return descripcion;
    }

    public int capturarCorrelativo(String idTipoC) {
        sql = "SELECT TOP 1 NRO_COMPROBANTE+1 "
                + "FROM PAGO WHERE ID_TIPO_COMPROBANTE='"+idTipoC+"' "
                + "ORDER BY NRO_COMPROBANTE DESC";
        int nroComp = 0;

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                nroComp = rs.getInt(1);
            }

        } catch (Exception e) {
        }

        return nroComp;
    }
    
}
