package Dao;

import Utilidades.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class TipoPagoDao {

    Connection cn = Conexion.getConnection();
    String sql = "";

    public String capturarIdEstado(String descripcion) {

        sql = "SELECT ID_TIPO_PAGO FROM TIPO_PAGO WHERE DESCRIPCION='" + descripcion + "'";
        String idTipoPago = "";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                idTipoPago = rs.getString("ID_TIPO_PAGO");
            }

        } catch (Exception e) {
        }

        return idTipoPago;
    }

    public String capturarDescripcion(String idTipoPago) {

        sql = "SELECT DESCRIPCION FROM TIPO_PAGO WHERE ID_TIPO_PAGO='" + idTipoPago + "'";
        String descripcion = "";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                descripcion = rs.getString("DESCRIPCION");
            }

        } catch (Exception e) {
        }

        return descripcion;
    }

    public int capturarCorrelativo(String idTipoPago) {
        sql = "SELECT TOP 1 NRO_PAGO+1 "
                + "FROM PAGO WHERE ID_TIPO_PAGO='"+idTipoPago+"' "
                + "ORDER BY NRO_PAGO DESC";
        int nroPago = 0;

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                nroPago = rs.getInt(1);
            }

        } catch (Exception e) {
        }

        return nroPago;
    }

}
