package Dao;

import Datos.Habitacion;
import Interfaces.IHabitacion;
import Utilidades.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class HabitacionDao implements IHabitacion {

    Connection cn = Conexion.getConnection();
    String sql = "";
    public Integer totalRegistros = 0;

    @Override
    public DefaultTableModel mostrar(String buscar) {

        DefaultTableModel modelo = null;

        String[] titulos = {"ID", "Tipo", "Número", "Piso", "Descripción", "Estado"};
        String[] registros = new String[6];

        modelo = new DefaultTableModel(null, titulos);
        sql = "SELECT H.ID_HABITACION,TH.DESCRIPCION,H.NUMERO,H.PISO,H.DESCRIPCION,E.DESCRIPCION "
                + "FROM HABITACION H "
                + "INNER JOIN TIPO_HABITACION TH ON TH.ID_TIPO_HABITACION=H.ID_TIPO_HABITACION "
                + "INNER JOIN ESTADO E ON E.ID_ESTADO=H.ID_ESTADO "
                + "WHERE H.NUMERO like '%" + buscar + "%' AND H.ID_ESTADO!='13' ORDER BY H.ID_HABITACION,H.PISO";

        try {

            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                registros[0] = rs.getString(1);
                registros[1] = rs.getString(2);
                registros[2] = rs.getString(3);
                registros[3] = rs.getString(4);
                registros[4] = rs.getString(5);
                registros[5] = rs.getString(6);

                totalRegistros = totalRegistros + 1;
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;
        }
    }

    @Override
    public DefaultTableModel mostrarVista(String numero) {
        // HABITACIONES DISPOBNIBLES
        
        DefaultTableModel modelo = null;

        String[] titulos = {"Reserva", "Cliente", "Item", "Número", "F.Ingreso", "F.Salida","Estado"};
        String[] registros = new String[7];

        modelo = new DefaultTableModel(null, titulos);
        sql = "SELECT RDET.ID_RESERVA,C.APEPATERNO+', '+C.NOMBRE,RDET.ITEM,H.NUMERO,RDET.FECHA_INGRESO,RDET.FECHA_SALIDA,E.DESCRIPCION " +
                "FROM DETALLE_RESERVA RDET " +
                "INNER JOIN RESERVA R ON R.ID_RESERVA=RDET.ID_RESERVA " +
                "INNER JOIN CLIENTE C ON C.ID_CLIENTE=R.ID_CLIENTE " +
                "INNER JOIN HABITACION H ON H.ID_HABITACION=RDET.ID_HABITACION AND NUMERO='"+numero+"' " +
                "INNER JOIN ESTADO E ON E.ID_ESTADO=RDET.ID_ESTADO AND E.ID_ESTADO='01' " +
                "WHERE RDET.FECHA_INGRESO>GETDATE() OR RDET.FECHA_SALIDA>GETDATE()";

        try {

            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                registros[0] = rs.getString(1);
                registros[1] = rs.getString(2);
                registros[2] = rs.getString(3);
                registros[3] = rs.getString(4);
                registros[4] = rs.getString(5);
                registros[5] = rs.getString(6);
                registros[6] = rs.getString(7);

                totalRegistros = totalRegistros + 1;
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;
        }
        
    }

    @Override
    public DefaultComboBoxModel mostrarTipoHabitacion() {

        sql = "SELECT * FROM TIPO_HABITACION";
        DefaultComboBoxModel cbox = new DefaultComboBoxModel();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cbox.addElement(rs.getString("DESCRIPCION"));
            }

        } catch (Exception e) {
        }

        return cbox;

    }

    @Override
    public DefaultComboBoxModel mostrarEstadoHabitacion() {

        sql = "SELECT * FROM ESTADO WHERE ID_ESTADO IN ('10','11','12')";
        DefaultComboBoxModel cbox = new DefaultComboBoxModel();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cbox.addElement(rs.getString("DESCRIPCION"));
            }

        } catch (Exception e) {
        }

        return cbox;
    }

    @Override
    public boolean insertar(Habitacion hab) {

        sql = "INSERT INTO habitacion "
                + " VALUES (?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, hab.getIdHabitacion());
            pst.setString(2, hab.getTipo_habitacion());
            pst.setInt(3, hab.getNumero());
            pst.setInt(4, hab.getPiso());
            pst.setString(5, hab.getEstado());
            pst.setString(6, hab.getDescripcion());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }

    }

    @Override
    public boolean editar(Habitacion hab) {
        sql = "UPDATE HABITACION SET ID_TIPO_HABITACION=?,NUMERO=?,PISO=?,ID_ESTADO=?,DESCRIPCION=?"
                + " WHERE ID_HABITACION=?";

        try {

            PreparedStatement pst = cn.prepareStatement(sql);

            pst.setString(1, hab.getTipo_habitacion());
            pst.setInt(2, hab.getNumero());
            pst.setInt(3, hab.getPiso());
            pst.setString(4, hab.getEstado());
            pst.setString(5, hab.getDescripcion());
            pst.setString(6, hab.getIdHabitacion());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }

    }

    @Override
    public boolean eliminar(Habitacion hab) {
        sql = "UPDATE HABITACION SET ID_ESTADO='13' WHERE ID_HABITACION=?";

        try {

            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, hab.getIdHabitacion());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }
    }

    @Override
    public boolean desocupar(Habitacion hab) {
        sql = "UPDATE HABITACION SET ID_ESTADO='10'"
                + " WHERE ID_HABITACION=?";

        try {

            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, hab.getIdHabitacion());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }
    }

    @Override
    public boolean ocupar(Habitacion hab) {
        sql = "UPDATE HABITACION SET ID_ESTADO='12'"
                + " WHERE ID_HABITACION=?";

        try {

            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, hab.getIdHabitacion());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }
    }

    @Override
    public DefaultComboBoxModel mostrarHabitacionxTipo(String idTipoHab) {

        sql = "SELECT NUMERO"
                + " FROM HABITACION"
                + " WHERE ID_TIPO_HABITACION='" + idTipoHab + "'";
        DefaultComboBoxModel cbox = new DefaultComboBoxModel();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cbox.addElement(rs.getString("NUMERO"));
            }

        } catch (Exception e) {
        }

        return cbox;

    }

    @Override
    public double mostrarPrecioxTipoHab(String idTipoHab) {

        sql = "SELECT PRECIO"
                + " FROM TIPO_HABITACION"
                + " WHERE ID_TIPO_HABITACION='" + idTipoHab + "'";
        double precio = 0;

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                precio = rs.getDouble("PRECIO");
            }

        } catch (Exception e) {
        }

        return precio;

    }

    @Override
    public String capturarIdTipoHabitacion(String descripcion) {
        sql = "SELECT ID_TIPO_HABITACION FROM TIPO_HABITACION WHERE DESCRIPCION='" + descripcion + "'";
        String idTipo = "";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                idTipo = rs.getString("ID_TIPO_HABITACION");
            }

        } catch (Exception e) {
        }

        return idTipo;
    }

    @Override
    public String capturarIdHabitacion(String numero) {
        sql = "SELECT ID_HABITACION FROM HABITACION WHERE NUMERO='" + numero + "'";
        String idHab = "";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
                idHab = rs.getString("ID_HABITACION");
            }

        } catch (Exception e) {
        }

        return idHab;
    }

    @Override
    public boolean existeHabitacion(String idHab, int numero) {
        sql = "SELECT * FROM HABITACION WHERE ID_HABITACION = '"+idHab+"' OR NUMERO="+String.valueOf(numero);
        boolean flag = false;

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next() == true) {
               flag=true;
            }

        } catch (Exception e) {
        }

        return flag;
    }
    
}
