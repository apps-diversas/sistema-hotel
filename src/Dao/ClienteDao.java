

package Dao;

import Datos.Cliente;
import Interfaces.ICliente;
import Utilidades.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ClienteDao implements ICliente{

    Connection cn = Conexion.getConnection();
    String sql = "";
    public Integer totalRegistros = 0;
    
    @Override
    public DefaultTableModel mostrar(String buscar) {
        
        DefaultTableModel modelo = null;

        String[] titulos = {"ID", "DNI","Nombre","APaterno","AMaterno","Cliente","F.Nac.","Edad", "Direccion",
                                "Telefono", "Email", "idPais","Pais", "idEstado","Estado"};
        String[] registros = new String[15];

        modelo = new DefaultTableModel(null, titulos);
        sql = "SELECT C.ID_CLIENTE, C.DNI,C.NOMBRE,C.APEPATERNO, C.APEMATERNO,C.APEPATERNO+' '+C.APEMATERNO+', '+C.NOMBRE,C.FECHA_NACIMIENTO,"
                + "DATEDIFF(yy,C.FECHA_NACIMIENTO,GETDATE()),C.DIRECCION,C.TELEFONO,C.EMAIL,C.ID_PAIS,P.DESCRIPCION,C.ID_ESTADO,ES.DESCRIPCION "
                + "FROM CLIENTE C "
                + "INNER JOIN PAIS P ON P.ID_PAIS=C.ID_PAIS "
                + "INNER JOIN ESTADO ES ON ES.ID_ESTADO=C.ID_ESTADO "
                + "WHERE (C.APEPATERNO+' '+C.APEMATERNO+', '+C.NOMBRE) like '%" + buscar + "%' ORDER BY C.ID_CLIENTE";

        try {

            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                registros[0] = rs.getString(1);
                registros[1] = rs.getString(2);
                registros[2] = rs.getString(3);
                registros[3] = rs.getString(4);
                registros[4] = rs.getString(5);
                registros[5] = rs.getString(6);
                registros[6] = rs.getString(7);
                registros[7] = rs.getString(8);
                registros[8] = rs.getString(9);
                registros[9] = rs.getString(10);
                registros[10] = rs.getString(11);
                registros[11] = rs.getString(12);
                registros[12] = rs.getString(13);
                registros[13] = rs.getString(14);
                registros[14] = rs.getString(15);

                totalRegistros = totalRegistros + 1;
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;
        }
        
    }

    @Override
    public DefaultTableModel mostrarVista(String buscar) {
         
        DefaultTableModel modelo = null;

        String[] titulos = {"ID", "DNI","Nombre","APaterno","AMaterno","Cliente","F.Nac.","Edad", "Direccion",
                                "Telefono", "Email", "idPais","Pais", "idEstado","Estado"};
        String[] registros = new String[15];

        modelo = new DefaultTableModel(null, titulos);
        sql = "SELECT C.ID_CLIENTE, C.DNI,C.NOMBRE,C.APEPATERNO, C.APEMATERNO,C.APEPATERNO+' '+C.APEMATERNO+', '+C.NOMBRE,C.FECHA_NACIMIENTO,"
                + "DATEDIFF(yy,C.FECHA_NACIMIENTO,GETDATE()),C.DIRECCION,C.TELEFONO,C.EMAIL,C.ID_PAIS,P.DESCRIPCION,C.ID_ESTADO,ES.DESCRIPCION "
                + "FROM CLIENTE C "
                + "INNER JOIN PAIS P ON P.ID_PAIS=C.ID_PAIS "
                + "INNER JOIN ESTADO ES ON ES.ID_ESTADO=C.ID_ESTADO "
                + "WHERE (C.APEPATERNO+' '+C.APEMATERNO+', '+C.NOMBRE) like '%" + buscar + "%' AND C.ID_ESTADO!='02' ORDER BY C.ID_CLIENTE";

        try {

            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                registros[0] = rs.getString(1);
                registros[1] = rs.getString(2);
                registros[2] = rs.getString(3);
                registros[3] = rs.getString(4);
                registros[4] = rs.getString(5);
                registros[5] = rs.getString(6);
                registros[6] = rs.getString(7);
                registros[7] = rs.getString(8);
                registros[8] = rs.getString(9);
                registros[9] = rs.getString(10);
                registros[10] = rs.getString(11);
                registros[11] = rs.getString(12);
                registros[12] = rs.getString(13);
                registros[13] = rs.getString(14);
                registros[14] = rs.getString(15);

                totalRegistros = totalRegistros + 1;
                modelo.addRow(registros);
            }
            return modelo;
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;
        }
    }

    @Override
    public DefaultComboBoxModel mostrarPaisCliente() {
        sql = "SELECT * FROM PAIS";
        DefaultComboBoxModel cbox = new DefaultComboBoxModel();
        
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cbox.addElement(rs.getString("DESCRIPCION"));
            }

        } catch (Exception e) {
        }

        return cbox;
    }

    @Override
    public DefaultComboBoxModel mostrarEstadoCliente() {
        sql = "SELECT * FROM ESTADO WHERE ID_ESTADO IN ('01','02')";
        DefaultComboBoxModel cbox = new DefaultComboBoxModel();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cbox.addElement(rs.getString("DESCRIPCION"));
            }

        } catch (Exception e) {
        }

        return cbox;
    }

    @Override
    public boolean insertar(Cliente cli) {
        sql = "INSERT INTO CLIENTE "
                + " VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, cli.getIdCliente());
            pst.setString(2, cli.getDni());
            pst.setString(3, cli.getNombres());
            pst.setString(4, cli.getApaterno());
            pst.setString(5, cli.getAmaterno());
            pst.setDate(6, cli.getFecha_nacimiento());
            pst.setString(7, cli.getDireccion());
            pst.setString(8, cli.getTelefono());
            pst.setString(9, cli.getEmail());
            pst.setString(10, cli.getPais());
            pst.setString(11, cli.getEstado());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }
    }

    @Override
    public boolean editar(Cliente cli) {
        sql = "UPDATE CLIENTE SET DNI=?, NOMBRE=?, APEPATERNO=?, APEMATERNO=?, FECHA_NACIMIENTO=?, DIRECCION=?, TELEFONO=?," +
                " EMAIL=?, ID_PAIS=?, ID_ESTADO=? "
                + " WHERE ID_CLIENTE=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, cli.getDni());
            pst.setString(2, cli.getNombres());
            pst.setString(3, cli.getApaterno());
            pst.setString(4, cli.getAmaterno());
            pst.setDate(5, cli.getFecha_nacimiento());
            pst.setString(6, cli.getDireccion());
            pst.setString(7, cli.getTelefono());
            pst.setString(8, cli.getEmail());
            pst.setString(9, cli.getPais());
            pst.setString(10, cli.getEstado());
            
            pst.setString(11, cli.getIdCliente());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }
    }

    @Override
    public boolean eliminar(Cliente cli) {
        sql = "UPDATE CLIENTE SET ID_ESTADO='02' WHERE ID_CLIENTE=?";
        
        try {
            
           PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, cli.getIdCliente());
            
            int n = pst.executeUpdate();
            
            if (n!=0) {
                return true;
            }else{
                return false;
            } 
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }
    }

}
